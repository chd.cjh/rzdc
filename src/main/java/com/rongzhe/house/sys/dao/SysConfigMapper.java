package com.rongzhe.house.sys.dao;

import com.rongzhe.house.sys.entity.Options;
import com.rongzhe.house.sys.entity.SysConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by JackWangon[www.coder520.com] 14:01 2018/3/10.
 */
@Repository
@Mapper
public interface SysConfigMapper {


    List<Options<String>> selectConfig();

    SysConfig selectVersion(@Param("channel") String channel);

}
