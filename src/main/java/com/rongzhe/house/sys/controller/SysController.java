package com.rongzhe.house.sys.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.sys.service.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by JackWangon[www.coder520.com] 13:55 2018/3/10.
 */
@Controller
@RequestMapping("sys")
public class SysController {


    @Autowired
    private SysService sysService;

    @RequestMapping("/config")
    @ResponseBody
    public ApiResult config() {

        return sysService.config();

    }


    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ResponseBody
    public ApiResult version(@RequestParam(value = "channel") String channel,String version) {

        return sysService.version(channel,version);

    }


    @RequestMapping(value = "/apk", method = RequestMethod.GET)
    public void apk(HttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException {

        String userAgent = request.getHeader("user-agent");

        boolean status = userAgent.contains("Android");
        boolean status2 = userAgent.contains("iPhone");

        if (userAgent.indexOf("micromessenger") > -1) {//微信客户端
            //TODO

        } else {

        }
        httpServletResponse.setContentType("text/html");
        PrintWriter printWriter = httpServletResponse.getWriter();

        if (status == true) {
            printWriter.write("<script>window.location.href ='http://a.app.qq.com/o/simple.jsp?pkgname=com.rongzhe.house' </script>");
        }

        if (status2 == true) {
//            return "redirect:http://itunes.apple.com/cn/app/true/id1355558342?mt=8";

            printWriter.write("<script>window.location.href ='" + "https://itunes.apple.com/cn/app/true/id1355558342?mt=8" + "' </script>");
//            return "redirect:https://itunes.apple.com/cn/app/true/id1355558342?mt=8";
        }

        printWriter.close();
        return;


    }


}
