package com.rongzhe.house.sys.entity;


import lombok.Data;

import java.util.List;

@Data
public class Options<T> {


    private String label;

    private T value;

    private List<Options<T>> options;

    private List<Options<T>> children;

}
