package com.rongzhe.house.sys.entity;

import lombok.Data;

import java.util.Date;

/**
 * <p>Description: 系统配置   </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author yangpeng
 * @version 1.0
 * @since 2018-03-09
 */
@Data
public class SysConfig {

    private Long id;
    /**
     * 扩展字段
     */
    private String ext;
    /**
     * 编码
     */
    private String code;
    /**
     * 操作
     */
    private String action;
    /**
     * 名称
     */
    private String name;
    /**
     * 插入时间
     */

    private Date insertTime;
    /**
     * 插入人uid
     */
    private Integer insertUid;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改人uid
     */

    private Integer updateUid;


}
