package com.rongzhe.house.sys.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.contract.entity.SysVersion;
import com.rongzhe.house.contract.service.SysVersionService;
import com.rongzhe.house.sys.dao.SysConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by JackWangon[www.coder520.com] 13:58 2018/3/10.
 */
@Service
public class SysServiceImpl implements SysService {


    @Autowired
    SysConfigMapper sysConfigMapper;

    @Autowired
    SysVersionService sysVersionService;

    @Override
    public ApiResult config() {
        return new ApiResult(sysConfigMapper.selectConfig());
    }

    @Override
    public ApiResult<SysVersion> version(String channel,String version) {

        SysVersion sysVersion = sysVersionService.selectOne(new EntityWrapper<SysVersion>()
                .eq("name", channel)
                .eq("version", version)
        );

    /*    sysVersion.setParam(JSON.parseObject(sysVersion.getExt(), Map.class));
        sysVersion.setExt(null);*/

        return new ApiResult(sysVersion);
    }

    @Override
    public String apk() {

        SysVersion sysVersion = sysVersionService.selectOne(new EntityWrapper<SysVersion>()
                .eq("name", "all")
                .eq("current_state", 1)
        );

        sysVersion.setParam(JSON.parseObject(sysVersion.getExt(), Map.class));

        return (String) sysVersion.getParam().get("downloadUrl");
    }
}
