package com.rongzhe.house.sys.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.contract.entity.SysVersion;

/**
 * Created by JackWangon[www.coder520.com] 13:57 2018/3/10.
 */
public interface SysService {


    ApiResult config();

    ApiResult<SysVersion> version(String channel,String version);

    String apk();
}
