package com.rongzhe.house.repair.service;

import com.rongzhe.house.repair.entity.HouseRepairImage;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 报修图片 服务类
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
public interface HouseRepairImageService extends IService<HouseRepairImage> {

}
