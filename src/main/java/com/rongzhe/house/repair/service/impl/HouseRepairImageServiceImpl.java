package com.rongzhe.house.repair.service.impl;

import com.rongzhe.house.repair.entity.HouseRepairImage;
import com.rongzhe.house.repair.mapper.HouseRepairImageMapper;
import com.rongzhe.house.repair.service.HouseRepairImageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报修图片 服务实现类
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Service
public class HouseRepairImageServiceImpl extends ServiceImpl<HouseRepairImageMapper, HouseRepairImage> implements HouseRepairImageService {

}
