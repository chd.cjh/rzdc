package com.rongzhe.house.repair.service;

import com.baomidou.mybatisplus.service.IService;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.repair.criteria.HouseRepairCriteria;
import com.rongzhe.house.repair.entity.HouseRepair;

import java.util.List;

/**
 * <p>
 * 报修表 服务类
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
public interface HouseRepairService extends IService<HouseRepair> {

    /**
     * 查询保修列表
     * @param houseRepairCriteria
     * @param currentUserId
     * @return
     */
    List<HouseRepairCriteria> queryUserRepair(HouseRepairCriteria houseRepairCriteria,Integer currentUserId);

    /**
     * 添加报修
     * @param houseRepairCriteria
     * @param currentUserId
     * @return
     */
    ApiResult insertOrUpdateHouseRepair(HouseRepairCriteria houseRepairCriteria,Integer currentUserId);


    /**
     * 查询报修详情
     * @param houseRepairCriteria
     * @param currentUserId
     * @return
     */
    ApiResult queryUserRepairDetail(HouseRepairCriteria houseRepairCriteria,Integer currentUserId);
}
