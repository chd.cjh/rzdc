package com.rongzhe.house.repair.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.utils.PageUtils;
import com.rongzhe.house.repair.criteria.HouseRepairCriteria;
import com.rongzhe.house.repair.entity.HouseRepair;
import com.rongzhe.house.repair.entity.HouseRepairImage;
import com.rongzhe.house.repair.mapper.HouseRepairMapper;
import com.rongzhe.house.repair.service.HouseRepairImageService;
import com.rongzhe.house.repair.service.HouseRepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 报修表 服务实现类
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Service
public class HouseRepairServiceImpl extends ServiceImpl<HouseRepairMapper, HouseRepair> implements HouseRepairService {

    @Autowired
    private HouseRepairMapper houseRepairMapper;

    @Autowired
    private HouseRepairImageService houseRepairImageService;

    @Override
    public List<HouseRepairCriteria> queryUserRepair(HouseRepairCriteria houseRepairCriteria,Integer currentUserId) {
        houseRepairCriteria.setLiveId(currentUserId);
        /*Long count = houseRepairMapper.selectHouseRepairCount(houseRepairCriteria);
        if(null==count || count==0){
            return null;
        }*/
        PageUtils.initPageParam(houseRepairCriteria);
        List<HouseRepairCriteria> list = houseRepairMapper.selectHouseRepairList(houseRepairCriteria);
        return list;
    }

    @Transactional
    @Override
    public ApiResult insertOrUpdateHouseRepair(HouseRepairCriteria houseRepairCriteria,Integer currentUserId) {
        ApiResult result=new ApiResult();
        result.setCode(-1);
        result.setMessage("操作失败");
        if(houseRepairCriteria.getHouseId()==null||houseRepairCriteria.getHouseId()==0){
            return result;
        }
        HouseRepair houseRepair=new HouseRepair(
                houseRepairCriteria.getId()==null?null:houseRepairCriteria.getId(),
                houseRepairCriteria.getLiveId()==null?currentUserId:houseRepairCriteria.getLiveId(),
                houseRepairCriteria.getDescript(),
                houseRepairCriteria.getStatus()==null?1:houseRepairCriteria.getStatus(),
                null,
                houseRepairCriteria.getSource(),
                houseRepairCriteria.getReason(),
                houseRepairCriteria.getPreTime(),
                (houseRepairCriteria.getId()==null?new Date():houseRepairCriteria.getInsertTime()),
                (houseRepairCriteria.getId()==null?currentUserId:houseRepairCriteria.getInsertUid()),
                new Date(),
                houseRepairCriteria.getUpdateUid()==null?currentUserId:houseRepairCriteria.getUpdateUid(),
                houseRepairCriteria.getHouseId(),
                houseRepairCriteria.getUserName(),
                houseRepairCriteria.getPhone()

        );
        boolean flag = insertOrUpdate(houseRepair);


        if(houseRepairCriteria.getId()==null){//新增录入图片
            //报修图片
            String urlStr = houseRepairCriteria.getUrl();
            String[] urls=null;
            if(null!=urlStr&&!urlStr.equals("")){
                urls = urlStr.split(",");
            }
            List<HouseRepairImage> repairImages=new ArrayList<>();
            if(null!=urls&&urls.length>0){
                for (String url:urls) {
                    HouseRepairImage repairImage=new HouseRepairImage();
                    repairImage.setUrl(url);
                    repairImage.setInsertUid(currentUserId);
                    repairImage.setRepairId(houseRepair.getId());
                    repairImage.setInsertTime(new Date());
                    repairImages.add(repairImage);
                }
                boolean batch = houseRepairImageService.insertBatch(repairImages);
            }

        }
        if(flag){
            result.setCode(200);
            result.setMessage("操作成功");
        }
        return result;
    }

    @Override
    public ApiResult queryUserRepairDetail(HouseRepairCriteria houseRepairCriteria, Integer currentUserId) {
        houseRepairCriteria.setLiveId(currentUserId);
        PageUtils.initPageParam(houseRepairCriteria);
        List<HouseRepairCriteria> list = houseRepairMapper.selectHouseRepairList(houseRepairCriteria);
        if(null!=list&&list.size()>0){
            return ApiResult.buildOk(list.get(0));
        }else{
            return ApiResult.buildFail("无数据");
        }

    }
}
