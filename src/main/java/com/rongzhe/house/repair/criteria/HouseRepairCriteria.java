package com.rongzhe.house.repair.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import com.rongzhe.house.repair.entity.HouseRepairImage;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author Chengjia
 * @date 2018/4/25 14:22
 * 报修服务查询信息
 */
@Data
public class HouseRepairCriteria extends BaseParam {

    private String userName;

    private String phone;

    /**
     * 租客姓名
     */
    private String realname;
    /**
     * 租客电话
     */
    private String mobile;

    /**
     * 房源ID
     */
    private Integer houseId;

    /**
     * 房源名称
     */
    private String houseName;

    /**
     * 分店
     */
    private String branName;

    /**
     * 开始时间
     */
    private Date beginDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 报修照片
     */
    private List<HouseRepairImage> imgs;

    private Integer id;
    /**
     * 报修人id
     */
    private Integer liveId;
    /**
     * 描述
     */
    private String descript;
    /**
     * 处理状态 0 待处理 1处理中 2 处理完成 3已撤销 4已拒绝
     */
    private Integer status;
    /**
     * 备注
     */
    private String note;
    /**
     * 报修来源
     */
    private String source;
    /**
     * 拒绝原因
     */
    private String reason;
    /**
     * 预处理时间
     */
    private Date preTime;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private String url;

}
