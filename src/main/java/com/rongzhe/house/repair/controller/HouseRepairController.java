package com.rongzhe.house.repair.controller;


import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.rest.BaseController;
import com.rongzhe.house.repair.criteria.HouseRepairCriteria;
import com.rongzhe.house.repair.service.HouseRepairService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 报修表 前端控制器
 * </p>
 *
 * @author chengjia123
 * @since 2018-04-25
 */
@RestController
@RequestMapping("/repair")
public class HouseRepairController extends BaseController{

    @Autowired
    private HouseRepairService houseRepairService;

    @ApiOperation(value = "报修列表", notes = "houseRepairCriteria", httpMethod = "POST")
    @RequestMapping("/search")
    public ApiResult<String> queryUserRepair(@RequestBody HouseRepairCriteria houseRepairCriteria){
        return ApiResult.buildOk(houseRepairService.queryUserRepair(houseRepairCriteria,getCurrentUser().getUserId()));
    }


    @ApiOperation(value = "报修新增", notes = "houseRepairCriteria", httpMethod = "POST")
    @RequestMapping("/saveOrUpdate")
    public ApiResult saveOrUpdate(@RequestBody HouseRepairCriteria houseRepairCriteria){
        return houseRepairService.insertOrUpdateHouseRepair(houseRepairCriteria,getCurrentUser().getUserId());
    }

    @ApiOperation(value = "报修详情", notes = "houseRepairCriteria", httpMethod = "POST")
    @RequestMapping("/detail")
    public ApiResult detail(@RequestBody HouseRepairCriteria houseRepairCriteria){
        return houseRepairService.queryUserRepairDetail(houseRepairCriteria,getCurrentUser().getUserId());
    }

}

