package com.rongzhe.house.repair.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 报修图片 前端控制器
 * </p>
 *
 * @author chengjia123
 * @since 2018-04-25
 */
@RestController
@RequestMapping("/repairImage")
public class HouseRepairImageController {

}

