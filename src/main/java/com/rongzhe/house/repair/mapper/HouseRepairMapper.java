package com.rongzhe.house.repair.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.rongzhe.house.repair.criteria.HouseRepairCriteria;
import com.rongzhe.house.repair.entity.HouseRepair;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 报修表 Mapper 接口
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Mapper
public interface HouseRepairMapper extends BaseMapper<HouseRepair> {
    /**
     * 个人维修记录
     * @param houseRepairCriteria
     * @return
     */
    List<HouseRepairCriteria> selectHouseRepairList(HouseRepairCriteria houseRepairCriteria);
    /**
     * 维修记录数
     * @param houseRepairCriteria
     * @return
     */
    Long selectHouseRepairCount(HouseRepairCriteria houseRepairCriteria);
}
