package com.rongzhe.house.repair.mapper;

import com.rongzhe.house.repair.entity.HouseRepairImage;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 报修图片 Mapper 接口
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Mapper
public interface HouseRepairImageMapper extends BaseMapper<HouseRepairImage> {

}
