package com.rongzhe.house.repair.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 报修图片
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Data
@Accessors(chain = true)
@TableName("house_repair_image")
public class HouseRepairImage extends Model<HouseRepairImage> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 报修表id
     */
    @TableField("repair_id")
    private Integer repairId;
    /**
     * 报修图片地址
     */
    private String url;
    @TableField("insert_time")
    private Date insertTime;
    @TableField("insert_uid")
    private Integer insertUid;
    @TableField("update_time")
    private Date updateTime;
    @TableField("update_uid")
    private Integer updateUid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
