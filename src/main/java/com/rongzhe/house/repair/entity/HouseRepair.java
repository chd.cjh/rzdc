package com.rongzhe.house.repair.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 报修表
 * </p>
 *
 * @author chengjia
 * @since 2018-04-26
 */
@Data
@Accessors(chain = true)
@TableName("house_repair")
public class HouseRepair extends Model<HouseRepair> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 报修人id
     */
    @TableField("live_id")
    private Integer liveId;

    /**
     * 房源ID
     */
    @TableField("house_id")
    private Integer houseId;
    /**
     * 描述
     */
    private String descript;
    /**
     * 处理状态 0 待处理 1处理中 2 处理完成 3已撤销 4已拒绝
     */
    private Integer status;
    /**
     * 备注
     */
    private String note;
    /**
     * 报修来源
     */
    private String source;
    /**
     * 拒绝原因
     */
    private String reason;

    @TableField("user_name")
    private String userName;

    private String phone;
    /**
     * 预处理时间
     */
    @TableField("pre_time")
    private Date preTime;
    @TableField("insert_time")
    private Date insertTime;
    @TableField("insert_uid")
    private Integer insertUid;
    @TableField("update_time")
    private Date updateTime;
    @TableField("update_uid")
    private Integer updateUid;

    @TableField(exist = false)
    private List<HouseRepairImage> imgs;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public HouseRepair() {

    }

    public HouseRepair(Integer id, Integer liveId, String descript, Integer status, String note, String source, String reason, Date preTime, Date insertTime, Integer insertUid, Date updateTime, Integer updateUid,Integer houseId,String userName,String phone) {
        this.id = id;
        this.liveId = liveId;
        this.descript = descript;
        this.status = status;
        this.note = note;
        this.source = source;
        this.reason = reason;
        this.preTime = preTime;
        this.insertTime = insertTime;
        this.insertUid = insertUid;
        this.updateTime = updateTime;
        this.updateUid = updateUid;
        this.houseId=houseId;
        this.userName=userName;
        this.phone=phone;
    }
}
