package com.rongzhe.house.position.service;

import com.rongzhe.house.house.dao.HousePositionCityMapper;
import com.rongzhe.house.house.entity.HousePositionCity;
import com.rongzhe.house.position.dao.HousePositionCountyMapper;
import com.rongzhe.house.position.dao.HousePositionProvinceMapper;
import com.rongzhe.house.position.dao.HouseRouteMapper;
import com.rongzhe.house.position.entity.HousePositionCounty;
import com.rongzhe.house.position.entity.HousePositionProvince;
import com.rongzhe.house.position.entity.HouseRoute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hggxg on 2017/11/18.
 */
@Service
public class PositionServiceImpl implements PositionService {


    @Autowired
    private HousePositionProvinceMapper housePositionProvinceMapper;

    @Autowired
    private HousePositionCountyMapper housePositionCountyMapper;

    @Autowired
    protected HouseRouteMapper houseRouteMapper;

    @Autowired
    private HousePositionCityMapper housePositionCityMapper;

    @Override
    public List<HousePositionProvince> getProvinceList() {

        return housePositionProvinceMapper.selectProvinceList();

    }

    @Override
    public List<HousePositionCity> getCityList() {
        return housePositionCityMapper.selectList();
    }

    @Override
    public List<HousePositionCounty> getCountyListByProvinceId(String provinceId) {


        return housePositionCountyMapper.selectCountyListByProvinceId(provinceId);
    }

    @Override
    public List<HouseRoute> getHouseRouteListByProvinceId(String provinceId) {
        return houseRouteMapper.selectHouseRouteListByProvinceId(provinceId);
    }

    @Override
    public List<HousePositionCounty> getCountyListByCityId(String cityId) {
        return housePositionCountyMapper.selectCountyListByProvinceId(cityId);
    }

    @Override
    public List<HouseRoute> getHouseRouteListByCityId(String cityId) {
        return houseRouteMapper.selectHouseRouteListByProvinceId(cityId);
    }


}
