package com.rongzhe.house.position.service;

import com.rongzhe.house.house.entity.HousePositionCity;
import com.rongzhe.house.position.entity.HousePositionCounty;
import com.rongzhe.house.position.entity.HousePositionProvince;
import com.rongzhe.house.position.entity.HouseRoute;

import java.util.List;

/**
 * Created by hggxg on 2017/11/18.
 */
public interface PositionService {

    /**
     * 获取省
     *
     * @return
     */
    List<HousePositionProvince> getProvinceList();


    /**
     * 获取省
     *
     * @return
     */
    List<HousePositionCity> getCityList();


    /**
     * 获取 区 县
     *
     * @param provinceId
     * @return
     */
    List<HousePositionCounty> getCountyListByProvinceId(String provinceId);


    /**
     * 根据省获取地铁
     *
     * @param provinceId
     * @return
     */
    List<HouseRoute> getHouseRouteListByProvinceId(String provinceId);


    /**
     * 获取 区 县
     *
     * @param provinceId
     * @return
     */
    List<HousePositionCounty> getCountyListByCityId(String provinceId);


    /**
     * 根据城市获取地铁
     *
     * @param provinceId
     * @return
     */
    List<HouseRoute> getHouseRouteListByCityId(String provinceId);
    

}
