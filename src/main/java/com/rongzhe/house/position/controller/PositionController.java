package com.rongzhe.house.position.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.position.entity.HousePositionCounty;
import com.rongzhe.house.position.entity.HousePositionProvince;
import com.rongzhe.house.position.entity.HouseRoute;
import com.rongzhe.house.position.service.PositionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by hggxg on 2017/11/18.
 */
@Slf4j
@RestController
@RequestMapping("position")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @ApiOperation(value = "获取城市", notes = "位置", httpMethod = "GET")
    @RequestMapping(value = "/city", method = RequestMethod.GET)
    public ApiResult<List<HousePositionProvince>> provinceList() {

        ApiResult apiResult = new ApiResult();


        apiResult.setData(positionService.getCityList());


        return apiResult;

    }

    @ApiOperation(value = "根据城市获取区县", notes = "位置", httpMethod = "GET")
    @RequestMapping(value = "/county", method = RequestMethod.GET)
    @ApiImplicitParam(name = "cityId", value = "检索条件", required = true, dataType = "String")
    public ApiResult<List<HousePositionCounty>> countyList(@RequestParam(value = "cityId") String provinceId) {
        ApiResult apiResult = new ApiResult();

        apiResult.setData(positionService.getCountyListByCityId(provinceId));


        return apiResult;
    }


    @ApiOperation(value = "地铁", notes = "位置", httpMethod = "GET")
    @RequestMapping(value = "/route", method = RequestMethod.GET)
    @ApiImplicitParam(name = "cityId", value = "检索条件", required = true, dataType = "String")
    public ApiResult<List<HouseRoute>> routeList(@RequestParam(value = "cityId") String provinceId) {
        ApiResult apiResult = new ApiResult();

        apiResult.setData(positionService.getHouseRouteListByCityId(provinceId));


        return apiResult;
    }
}
