package com.rongzhe.house.position.dao;

import com.rongzhe.house.position.entity.HousePositionCounty;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface HousePositionCountyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HousePositionCounty record);

    int insertSelective(HousePositionCounty record);

    HousePositionCounty selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HousePositionCounty record);

    int updateByPrimaryKey(HousePositionCounty record);

    List<HousePositionCounty> selectCountyListByProvinceId(String provinceId);
}