package com.rongzhe.house.position.dao;

import com.rongzhe.house.position.entity.HouseRoute;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HouseRouteMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseRoute record);

    int insertSelective(HouseRoute record);

    HouseRoute selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseRoute record);

    int updateByPrimaryKey(HouseRoute record);

    List<HouseRoute> selectHouseRouteListByProvinceId(String provinceId);
}