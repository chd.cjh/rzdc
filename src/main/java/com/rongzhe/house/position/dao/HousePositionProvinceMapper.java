package com.rongzhe.house.position.dao;

import com.rongzhe.house.position.entity.HousePositionProvince;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface HousePositionProvinceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HousePositionProvince record);

    int insertSelective(HousePositionProvince record);

    HousePositionProvince selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HousePositionProvince record);

    int updateByPrimaryKey(HousePositionProvince record);

    List<HousePositionProvince> selectProvinceList();
}