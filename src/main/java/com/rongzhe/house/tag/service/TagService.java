package com.rongzhe.house.tag.service;

import com.rongzhe.house.tag.entity.Tag;

import java.util.List;

/**
 * Created by hggxg on 2017/12/5.
 */
public interface TagService {


    List<Tag> getTagListByType(Integer type);

    List<Tag> getAll();
}
