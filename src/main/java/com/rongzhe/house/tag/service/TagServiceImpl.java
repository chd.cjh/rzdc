package com.rongzhe.house.tag.service;

import com.rongzhe.house.tag.dao.TagMapper;
import com.rongzhe.house.tag.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hggxg on 2017/12/5.
 */
@Service
public class TagServiceImpl implements TagService {


    @Autowired
    private TagMapper tagMapper;

    @Override
    public List<Tag> getTagListByType(Integer type) {

        return tagMapper.selectByType(type);

    }

    @Override
    public List<Tag> getAll() {
        return tagMapper.selectAll();
    }
}
