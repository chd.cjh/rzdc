package com.rongzhe.house.tag.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.tag.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hggxg on 2017/12/5.
 */

@RestController
@RequestMapping("tag")
public class TagController {


    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/getTagListByType", method = RequestMethod.GET)
    public ApiResult getTagListByType(@RequestParam(value = "type") Integer type) {

        ApiResult apiResult = new ApiResult();
        apiResult.setData(tagService.getTagListByType(type));

        return apiResult;

    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ApiResult getAll() {

        ApiResult apiResult = new ApiResult();
        apiResult.setData(tagService.getAll());

        return apiResult;

    }


}
