package com.rongzhe.house.common.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangpeng
 * @version 1.0
 * @Description
 * @date 2017/11/22
 */
public class Messages {

    public static final byte MEMEBER_STATUS_NORMAL = 1;
    public static final byte MEMEBER_STATUS_DISABLE = 2;
    public static final String MEMBER_MESSAGE_DISALBE = "被禁止登陆，请联系管理人员";

    public static final String MESSAGE_CONNECTION_FAIL = "服务器连接失败，请重新刷新";
    public static final String MESSAGE_TOKEN_FAIL = "获取token失败";
    public static final String MESSAGE_UNKNOW_WRONG = "系统发生异常";
    public static final String MESSAGE_SYSTEM_BUSY = "系统繁忙,请重试";
    public static final String MESSAGE_AUTH_FAIL = "对不起，您没有权限";
    public static final String MESSAGE_NO_TOKEN = "您尚未登陆,请登录后重试";
    public static final String MESSAGE_FAIL = "操作失败";
    public static final String MESSAGE_OK = "操作成功";
    public static final String MESSAGE_NO_PARAM = "账号或密码不能为空";
    public static final String MESSAGE_USERNAME_EXIST="账号不存在";
    public static final String MESSAGE_USER_BASE_ERROR = "账号或密码错误";
    public static final String MESSAGE_INSERT_USER = "对不起，该手机号已经注册";


    public static final Map<Integer, String> messageCode = new HashMap<Integer, String>() {
        {
            put(Constants.UNlOGIN, "未登录");
        }
    };


}
