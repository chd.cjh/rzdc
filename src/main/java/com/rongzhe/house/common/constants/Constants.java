package com.rongzhe.house.common.constants;

/**
 * Created by JackWangon[www.] 2017/7/29.
 */
public class Constants {
    /**用户token**/
    public static final String REQUEST_TOKEN_KEY = "user-token";
    /**客户端版本**/
    public static final String REQUEST_VERSION_KEY = "version";
    /**客户端平台 android/ios**/
    public static final String REQUEST_PLATFORM_KEY = "platform";

    public static final String REQUEST_TYPE_KEY = "type";
    /**自定义状态码 start**/
    public static final int RESP_STATUS_OK = 200;
    public static final int RESP_STATUS_RIDE_END = 201;

    public static final int RESP_STATUS_NOAUTH = 401;

    public static final int UNKNOWN_PAY_TYPE = 400001;

    public static final int RESP_STATUS_INTERNAL_ERROR = 500;

    public static final int RESP_STATUS_BADREQUEST = 400;

    public static final int UNlOGIN = 40301;
    /**自定义状态码 end**/

    /**阿里云SMS start**/
    public static final String ALSMS_ACCESS_KEY = "LTAIT6j12QgenBz4";

    public static final String ALSMS_ACCESS_SECRET = "l83d2OY9YfiUDYQMJo4AVqjTWFY5ye";

    public static final String ALSMS_DOMAIN = "dysmsapi.aliyuncs.com";
    public static final String ALSMS_MS_PRODUCT = "Dysmsapi";

    public static final String ALSMS_VERCODE_TPLID = "SMS_123535027";

    public static final String ALSMS_SIGNNAME = "荣者集团";

    /**阿里云SMS end**/


    /***七牛keys start****/
    public static final String QINIU_ACCESS_KEY="QBg13GNhv-NCAvNtHnLJW5JNSqPzmzkixSGKQYYo";

    public static final String QINIU_SECRET_KEY="-8G0Vp1MTe5oDPP_QJTMH4nD_PJNKp3NkVeUub4L";

    public static final String QINIU_HEAD_IMG_BUCKET_NAME="rongzhe-formal";

    public static final String QINIU_PROTOCOL="http://";

    public static final String QINIU_HEAD_IMG_BUCKET_URL="images.rzccq.com";
    public static final String QINIU_CONTRACT_BUCKET="rongzhe-contract";

    public static final String QINIU_CONTRACT_BUCKET_URL = "contract.rzccq.com";
    /***七牛keys end****/

    /**百度云推送 start**/
    public static final String BAIDU_YUN_PUSH_API_KEY="";

    public static final String BAIDU_YUN_PUSH_SECRET_KEY="";

    public static final String CHANNEL_REST_URL = "";
    /**百度云推送end**/



}
