package com.rongzhe.house.common.resp;


import com.rongzhe.house.common.constants.Constants;
import com.rongzhe.house.common.constants.Messages;
import lombok.Data;

/**
 * Created by JackWangon[www.] 2017/7/29.
 */
@Data
public class ApiResult<T> {


    private int code = Constants.RESP_STATUS_OK;

    private String message;

    private T data;


    public ApiResult() {
    }

    public ApiResult(T data) {
        this.data = data;
    }

    public ApiResult(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public static ApiResult buildOk() {
        return buildOk(Constants.RESP_STATUS_OK, Messages.MESSAGE_OK, null);
    }

    public static ApiResult buildOk(String message) {
        return buildOk(Constants.RESP_STATUS_OK, message, null);
    }

    public static ApiResult buildOk(Object data) {
        return buildOk(Constants.RESP_STATUS_OK, Messages.MESSAGE_OK, data);
    }

    public static ApiResult buildOk(String message, Object data) {
        return buildOk(Constants.RESP_STATUS_OK, message, data);
    }

    public static ApiResult buildOk(int code, String message, Object data) {
        return new ApiResult(code, message, data);
    }


    public static ApiResult buildFail() {
        return buildFail(Constants.RESP_STATUS_INTERNAL_ERROR, Messages.MESSAGE_FAIL, null);
    }

    public static ApiResult buildFail(String message) {
        return buildFail(Constants.RESP_STATUS_INTERNAL_ERROR, message, null);
    }

    public static ApiResult buildFail(String message, String data) {
        return buildFail(Constants.RESP_STATUS_INTERNAL_ERROR, message, data);
    }

    public static ApiResult buildFail(int code, String message, String data) {
        return new ApiResult(code, message, data);
    }

    public static ApiResult buildUnloginFail() {
        return new ApiResult(Constants.UNlOGIN);
    }

    public ApiResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
