package com.rongzhe.house.common;

import com.rongzhe.house.common.constants.Constants;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.utils.QiniuFileUploadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by hggxg on 2018/1/1.
 */
@Slf4j
@Controller
@RequestMapping("file")
public class FileController {

    @RequestMapping("/upload")
    @ResponseBody
    public ApiResult upload(@RequestParam MultipartFile file) throws RongZheBusinessException {

        try {
            ApiResult apiResult = new ApiResult();
            // 调用七牛
            String imgUrlName = Constants.QINIU_PROTOCOL + Constants.QINIU_HEAD_IMG_BUCKET_URL + "/" + QiniuFileUploadUtil.uploadHeadImg(file);
            apiResult.setData(imgUrlName);
            return apiResult;
        } catch (Exception e) {
            log.error("文件上传失败",e);
            throw new RongZheBusinessException("文件上传失败");
        }
    }

    @RequestMapping("/upload/list")
    @ResponseBody
    public ApiResult uploadList(@RequestParam MultipartFile[] files) throws RongZheBusinessException {

        try {
            ApiResult apiResult = new ApiResult();
            // 调用七牛


            List<String> imgUrlName = QiniuFileUploadUtil.uploadHeadImgList(files);


            apiResult.setData(imgUrlName);

            return apiResult;
        } catch (Exception e) {
            throw new RongZheBusinessException("文件上传失败");
        }
    }
}
