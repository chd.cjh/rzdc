package com.rongzhe.house.common;

import cn.jiguang.common.ClientConfig;
import cn.jpush.api.JPushClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by JackWangon[www.coder520.com] 14:17 2018/4/24.
 */
@Configuration
public class JpushConfig {

    @Value("${jpush.key}")
    private String key;

    @Value("${jpush.secret}")
    private String secret;


    @Bean
    public JPushClient jPushClient() {
        JPushClient jpushClient = new JPushClient(secret, key, null, ClientConfig.getInstance());

        return jpushClient;
    }


}
