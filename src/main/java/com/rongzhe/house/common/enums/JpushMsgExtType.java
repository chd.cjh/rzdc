package com.rongzhe.house.common.enums;

/**
 * Created by JackWangon[www.coder520.com] 17:20 2018/5/2.
 */
public enum JpushMsgExtType {

    //强制退出
    FORCE_EXIT("1", "账号已在另一台设备登录");

    private String type;

    /**
     * 可用占位符处理字符窜
     */
    private String content;

    JpushMsgExtType(String type, String content) {
        this.type = type;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}
