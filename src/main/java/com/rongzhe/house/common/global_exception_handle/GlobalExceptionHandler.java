package com.rongzhe.house.common.global_exception_handle;


import com.rongzhe.house.common.constants.Constants;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.exception.RongZheValidateException;
import com.rongzhe.house.common.resp.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {


    /**
     * 参数校验统一处理
     *
     * @param ex
     * @param httpServletResponse
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ApiResult handleInvalidArgument(MethodArgumentNotValidException ex, HttpServletResponse httpServletResponse) {


        log.error("框架参数校验错误", ex);

        List<ObjectError> errorList = ex.getBindingResult().getAllErrors();

        StringBuilder builder = new StringBuilder();
        errorList.forEach(objectError ->
                builder.append(objectError.getDefaultMessage()).append(",")

        );

        ApiResult apiResult = new ApiResult();
        apiResult.setCode(Constants.RESP_STATUS_BADREQUEST);
        apiResult.setMessage(builder.toString());
        httpServletResponse.setStatus(200);

        return apiResult;

    }

    @ExceptionHandler(RongZheValidateException.class)
    @ResponseBody
    public ApiResult handleRongZheValidateException(RongZheValidateException ex, HttpServletResponse httpServletResponse) {

        log.error("业务参数校验错误", ex);
        ApiResult apiResult = new ApiResult();
        apiResult.setCode(Constants.RESP_STATUS_BADREQUEST);
        apiResult.setMessage(ex.getMessage());
        httpServletResponse.setStatus(200);

        return apiResult;

    }


    @ExceptionHandler(RongZheBusinessException.class)
    @ResponseBody
    public ApiResult handleBizExp(HttpServletRequest request, Exception ex, HttpServletResponse response) {
        log.error("Business exception handler  " + ex.getMessage());
        ApiResult apiResult = new ApiResult();
        response.setStatus(200);
        apiResult.setCode(Constants.RESP_STATUS_INTERNAL_ERROR);
        apiResult.setMessage(ex.getMessage());

        return apiResult;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ApiResult handSql(Exception ex, HttpServletResponse response) {
        log.error("Unknown Exception " + ex);
        response.setStatus(200);
        ex.printStackTrace();
        ApiResult apiResult = new ApiResult();
        apiResult.setCode(Constants.RESP_STATUS_INTERNAL_ERROR);
        apiResult.setMessage("内部错误");
        return apiResult;
    }
}
