package com.rongzhe.house.common.rest;


import lombok.Data;

@Data
public class BaseParam {

    private int pageSize = 10;

    private int currentPage = 1;

    private int offset;

    private int limit;
}
