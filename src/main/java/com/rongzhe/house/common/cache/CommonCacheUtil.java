package com.rongzhe.house.common.cache;

import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.user.entity.UserElement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by JackWangon[www.coder520.com] 15:12 2017/11/11.
 */
@Component
@Slf4j
public class CommonCacheUtil {
    private static final String TOKEN_PREFIX = "admin.token.";

    private static final String USER_PREFIX = "user.";


    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 缓存 可以value 永久
     *
     * @param key
     * @param value
     */
    public void cache(String key, String value) {
        try {

            redisTemplate.opsForValue().set(key, value);

        } catch (Exception e) {
            log.error("Fail to cache value", e);
        }
    }

    /**
     * 获取缓存key
     *
     * @param key
     * @return
     */
    public String getCacheValue(String key) {
        String value = null;
        try {

            ValueOperations<String, String> operations = redisTemplate.opsForValue();
            value = operations.get(key);
        } catch (Exception e) {
            log.error("Fail to get cached value", e);
        }
        return value;
    }

    /**
     * 设置key value 以及过期时间
     *
     * @param key
     * @param value
     * @param expiry
     * @return
     */
    public long cacheNxExpire(String key, String value, int expiry) {
        long result = 0;
        try {
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, expiry, TimeUnit.MINUTES);
        } catch (Exception e) {
            log.error("Fail to cacheNx value", e);
        }

        return result;
    }

    /**
     * 删除缓存key
     *
     * @param key
     */
    public void delKey(String key) {


        try {
            redisTemplate.delete(key);
        } catch (Exception e) {
            log.error("Fail to remove key from redis", e);
        }
    }


    /**
     * 登录时设置token
     *
     * @param ue
     */
    public void putTokenWhenLogin(UserElement ue) {

        try {
            redisTemplate.delete(TOKEN_PREFIX + ue.getToken());
            redisTemplate.opsForHash().putAll(TOKEN_PREFIX + ue.getToken(), ue.toMap());

            redisTemplate.expire(TOKEN_PREFIX + ue.getToken(), 2592000, TimeUnit.SECONDS);

            redisTemplate.opsForSet().add(USER_PREFIX + ue.getUserId(), ue.getToken());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Fail to cache token to redis", e);
        }
    }


    public void remove(String token) {
        redisTemplate.delete(TOKEN_PREFIX + token);
    }

    /**
     * 缓存手机验证码专用 限制了发送次数
     *
     * @param key
     * @param value
     * @param type
     * @param timeout
     * @param ip
     * @return 1 当前验证码未过期   2 手机号超过当日验证码次数上限  3 ip超过当日验证码次数上线
     * @throws RongZheBusinessException
     */
    public int cacheForVerificationCode(String key, String value, String type, int timeout, String ip) throws RongZheBusinessException {
        try {

            ValueOperations<String, String> operations = redisTemplate.opsForValue();


            if (!lock(key)) {
                return 1;
            }


            String sendCount = operations.get(key + "." + type);
            try {
                if (sendCount != null && Integer.parseInt(sendCount) >= 10) {
                    redisTemplate.delete(key);
                    return 2;
                }
            } catch (NumberFormatException e) {
                log.error("Fail to process send count", e);
                redisTemplate.delete(key);
                return 2;
            }

            try {

                // trans.set(key, value);
                operations.set(key, value);

                redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
                long val = redisTemplate.opsForValue().increment(key + "." + type, 1);


                if (val == 1) {
                    redisTemplate.expire(key + "." + type, 86400, TimeUnit.SECONDS);
                }


            } catch (Exception e) {
                log.error("Fail to cache data into redis", e);
            }
        } catch (Exception e) {
            log.error("Fail to cache for expiry", e);
            throw new RongZheBusinessException("Fail to cache for expiry");
        }

        return 0;
    }


    private static final long LOCK_EXPIRE = 1000;

    public boolean lock(String k) {

        String key = "lock_" + k;

        return (Boolean) redisTemplate.execute((RedisCallback) connection -> {

            long expireAt = System.currentTimeMillis() + LOCK_EXPIRE + 1;
            Boolean acquire = connection.setNX(key.getBytes(), String.valueOf(expireAt).getBytes());


            if (acquire) {
                return true;
            } else {

                byte[] value = connection.get(key.getBytes());

                if (Objects.nonNull(value) && value.length > 0) {

                    long expireTime = Long.parseLong(new String(value));

                    if (expireTime < System.currentTimeMillis()) {
                        byte[] oldValue = connection.getSet(key.getBytes(), String.valueOf(System.currentTimeMillis() + LOCK_EXPIRE + 1).getBytes());

                        return Long.parseLong(new String(oldValue)) < System.currentTimeMillis();
                    }
                }
            }
            return false;
        });
    }


    /**
     * 根据token取缓存的用户信息
     *
     * @param token
     * @return
     * @throws RongZheBusinessException
     */
    public UserElement getUserByToken(String token) throws RongZheBusinessException {
        UserElement ue = null;

        HashOperations<String, String, String> operations = redisTemplate.opsForHash();
        try {


            Map<String, String> map = new HashMap<>();


            if (operations.get(TOKEN_PREFIX + token, "userId") == null) {
                //没有用户
                return null;
            }

            map.put("userId", operations.get(TOKEN_PREFIX + token, "userId"));
            map.put("token", operations.get(TOKEN_PREFIX + token, "token"));
            map.put("mobile", operations.get(TOKEN_PREFIX + token, "mobile"));


//            Map<String, String> map = jedisPool.getResource().hgetAll(TOKEN_PREFIX + token);
            if (map != null && !map.isEmpty()) {
                ue = UserElement.fromMap(map);
            } else {
                log.warn("Fail to find cached element for token {}", token);
            }
        } catch (Exception e) {
            log.error("Fail to get token from redis", e);
            throw new RongZheBusinessException("Fail to get token content");
        }

        return ue;
    }
}
