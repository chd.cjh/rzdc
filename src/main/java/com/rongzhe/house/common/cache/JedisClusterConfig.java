package com.rongzhe.house.common.cache;


import com.rongzhe.house.common.constants.Parameters;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

//@Configuration
@Slf4j
public class JedisClusterConfig {


    @Autowired
    private Parameters parameters;


    /**
     *  单例bean 可以注入使用
     * @return
     * @throws RongZheBusinessException
     */
    @Bean
    public JedisCluster getJedisCluster() throws RongZheBusinessException {
        try {

            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(parameters.getRedisMaxTotal());
            config.setMaxIdle(parameters.getRedisMaxIdle());
            config.setMaxWaitMillis(parameters.getRedisMaxWaitMillis());

            String[] serverArray = parameters.getRedisNode().split(",");
            Set<HostAndPort> nodes = new HashSet<>();
            for(String ipPortString : serverArray){
                String[] ipPortPair = ipPortString.split(":");
                nodes.add(new HostAndPort(ipPortPair[0].trim(),Integer.valueOf(ipPortPair[1].trim())));
            }

            return new JedisCluster(nodes,3000,3000,6,parameters.getRedisAuth(),config);

        } catch (Exception e) {
            log.error("Fail to initialize jedis cluster", e);
            throw new RongZheBusinessException("Fail to initialize jedis cluster");
        }
    }



}
