package com.rongzhe.house.common.utils;


import com.rongzhe.house.common.rest.BaseParam;

/**
 * <p>Description:    </p>
 * <p>email: 1033148062@qq.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author lgb
 * @version 1.0
 * @date 2017/12/22
 * @since 1.8
 */
public class PageUtils {


    /**
     * 配置分页
     * @param pageParam
     */
    public static void initPageParam(BaseParam pageParam){
        int offset = pageParam.getPageSize() * (pageParam.getCurrentPage() - 1);
        int limit = pageParam.getPageSize();
        pageParam.setOffset(offset);
        pageParam.setLimit(limit);
    }
}
