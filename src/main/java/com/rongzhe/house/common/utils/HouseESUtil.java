package com.rongzhe.house.common.utils;

import com.rongzhe.house.house.es_repository.ESHouse;
import com.rongzhe.house.house.service.HouseService;
import com.rongzhe.house.house.service.HouseServiceImpl;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * Created by JackWangon[www.coder520.com] 10:42 2018/4/2.
 */
@Component
public class HouseESUtil {

    @Autowired
    private Client client;


    public void updateHouseEsWrap(ESHouse esHouse) throws IOException, InterruptedException, ExecutionException {
        XContentBuilder xContentBuilder = jsonBuilder().startObject();


        if (esHouse.getHouseName() != null) {
            xContentBuilder.field("name", esHouse.getHouseName());
        }

        if (esHouse.getRegionName() != null) {

            xContentBuilder.field("countyName", esHouse.getRegionName());
        }
        if (esHouse.getCountyId() != null) {

            xContentBuilder.field("countyId", esHouse.getCountyId());
        }
        if (esHouse.getCityId() != null) {

            xContentBuilder.field("cityId", esHouse.getCityId());
        }
        if (esHouse.getEnterTime() != null) {

            xContentBuilder.field("enterTime", esHouse.getEnterTime());
        }

        if (esHouse.getRoomNum() != null) {

            xContentBuilder.field("roomNum", esHouse.getRoomNum());
        }
        if (esHouse.getRent() != null) {

            xContentBuilder.field("rent", esHouse.getRent());
        }
        if (esHouse.getLocation() != null) {

            if (esHouse.getLocation().get(0) != null && esHouse.getLocation().get(1) != null) {

                xContentBuilder.latlon("location", esHouse.getLocation().get(0), esHouse.getLocation().get(1));
            }
        }

        //.field("orientation")
        if (!CollectionUtils.isEmpty(esHouse.getRouteList())) {

            xContentBuilder.array("route", esHouse.getRouteList());
        }
        if (!CollectionUtils.isEmpty(esHouse.getRailList())) {

            xContentBuilder.array("railList", esHouse.getRailList());
        }
        if (!CollectionUtils.isEmpty(esHouse.getRailStationList())) {
            xContentBuilder.array("railStationList", esHouse.getRailStationList());

        }
        xContentBuilder.endObject();

        UpdateRequest updateRequest = new UpdateRequest(HouseServiceImpl.ES_HOUSE_INDEX, "condition_query", esHouse.getId().toString())
                .doc(xContentBuilder);

        UpdateResponse updateResponse = client.update(updateRequest).get();

        System.out.println(updateResponse);
    }

}
