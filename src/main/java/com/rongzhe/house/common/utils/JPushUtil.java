package com.rongzhe.house.common.utils;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import com.rongzhe.house.common.enums.JpushMsgExtType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by JackWangon[www.coder520.com] 14:34 2018/4/24.
 */
@Slf4j
@Component
public class JPushUtil {


    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(20);


    @Autowired
    private JPushClient jPushClient;

    public void sendWithRegisterIds(List<String> registerIds, Map<String, String> params, String content) throws APIConnectionException, APIRequestException {
        PushPayload pushPayload = PushPayload
                .newBuilder()
                //平台
                .setPlatform(Platform.all())
                //受众
                .setAudience(Audience.registrationId(registerIds))
                //消息
                .setMessage(
                        Message.newBuilder()
                                .setMsgContent(content)
                                .addExtras(params)
                                .build()
                )
                .build();
        send(pushPayload);
    }

    public void sendWithDevice(String device, Map<String, String> params, String content) throws APIConnectionException, APIRequestException {
        PushPayload pushPayload = PushPayload
                .newBuilder()
                //平台
                .setPlatform(Platform.all())
                //受众
//                .setAudience(Audience.alias(device))
                .setAudience(Audience.registrationId(device))
                //消息
                .setMessage(
                        Message.newBuilder()
                                .setMsgContent(content)
                                .addExtras(params)
                                .build()
                )
                .build();
        send(pushPayload);


    }

    private void send(PushPayload pushPayload) {
        EXECUTOR_SERVICE.execute(() -> {

            try {
                jPushClient.sendPush(pushPayload);
            } catch (APIConnectionException e) {
                e.printStackTrace();
                log.error("极光推送异常", e);
            } catch (APIRequestException e) {
                e.printStackTrace();
                log.error("极光推送异常", e);
            }
        });
    }

    public static void main(String[] args) {
        JPushClient jpushClient = new JPushClient("55abc3b8f33bbd16a9bf44bc", "bee119e74cd3f315cb25b13c", null, ClientConfig.getInstance());

        HashMap<String, String> type = new HashMap<String, String>() {
            {
                //下线消息
                put("type", JpushMsgExtType.FORCE_EXIT.getType());
            }
        };
        String content = JpushMsgExtType.FORCE_EXIT.getContent();
        PushPayload pushPayload = PushPayload
                .newBuilder()
                //平台
                .setPlatform(Platform.all())
                //受众
                .setAudience(Audience.registrationId("101d85590915cfc6cf6"))
                //消息
                .setMessage(
                        Message.newBuilder()
                                .setMsgContent(content)
                                .addExtras(type)
                                .build()
                )
                .build();
        try {
            jpushClient.sendPush(pushPayload);
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
//        EXECUTOR_SERVICE.execute(() -> {
//            try {
//                jpushClient.sendPush(pushPayload);
//            } catch (APIConnectionException e) {
//                e.printStackTrace();
//                log.error("极光推送异常", e);
//            } catch (APIRequestException e) {
//                e.printStackTrace();
//                log.error("极光推送异常", e);
//            }
//        });

    }


}
