package com.rongzhe.house.common;

import com.rongzhe.house.common.interceptor.TokenInterceptor;
import com.rongzhe.house.common.interceptor.VersionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by JackWangon[www.coder520.com] 21:29 2018/5/2.
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new VersionInterceptor());
        registry.addInterceptor(new TokenInterceptor());

        System.out.println("===========   拦截器注册完毕   ===========");
    }

}
