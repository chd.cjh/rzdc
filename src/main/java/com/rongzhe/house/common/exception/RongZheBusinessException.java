package com.rongzhe.house.common.exception;

/**
 * Created by JackWangon[www.coder520.com] 2017/7/31.
 */
public class RongZheBusinessException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -7370331410579650067L;

    public RongZheBusinessException(String message) {
        super(message);
    }

    public int getStatusCode() {
        return 500;
    }
}
