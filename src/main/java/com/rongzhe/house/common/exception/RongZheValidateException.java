package com.rongzhe.house.common.exception;

/**
 * Created by HeGang 14:56 2018/5/9.
 */
public class RongZheValidateException extends RuntimeException {

    public RongZheValidateException(String message) {
        super(message);
    }
}
