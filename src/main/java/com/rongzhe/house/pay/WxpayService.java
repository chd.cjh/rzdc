package com.rongzhe.house.pay;

import com.rongzhe.house.pay.entity.WeixinOrder;
import com.rongzhe.house.pay.entity.WeixinPayPost;

import java.util.Map;

/**
 * Created by hggxg on 2017/11/15.
 */
public interface WxpayService {
    Map<String, String> precreate(WeixinOrder weixinOrder);

    WeixinPayPost checkSign(Map<String, String> paramMap);
}
