package com.rongzhe.house.pay;

import lombok.Data;

import java.util.Date;

/**
 * Created by hggxg on 2017/11/15.
 */
@Data
public class AliPayPost {

    private String tradeNo;

    private String alipayTotalAmount;

    private String outTradeNo;

    private Date payDate;


}
