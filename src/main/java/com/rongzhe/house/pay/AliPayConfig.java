package com.rongzhe.house.pay;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by hggxg on 2017/11/15.
 */
@Component
@ConfigurationProperties
@Data
public class AliPayConfig {

    @Value("${pay.ali.timeoutExpress}")
    private String timeoutExpress;
    @Value("${pay.ali.alipayGateway}")
    private String alipayGateway;
    @Value("${pay.ali.notifyUrl}")
    private String notifyUrl;
    @Value("${pay.ali.sellerId}")
    private String sellerId;
    @Value("${pay.ali.appId}")
    private String appId;
    @Value("${pay.ali.appPublicKey}")
    private String appPublicKey;
    @Value("${pay.ali.appPrivateKey}")
    private String appPrivateKey;
    @Value("${pay.ali.alipayPublicKey}")
    private String alipayPublicKey;
    @Value("${pay.ali.aesKey}")
    private String aesKey;
    @Value("${pay.ali.charset}")
    private String charset;
    @Value("${pay.ali.pid}")
    private String pid;

}
