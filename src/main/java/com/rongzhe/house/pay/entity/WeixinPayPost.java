package com.rongzhe.house.pay.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by hggxg on 2017/11/15.
 */
@Data
public class WeixinPayPost {

    private String outTradeNo;
    private String transactionId;
    private Date timeEnd;
    private String cashFee;

}
