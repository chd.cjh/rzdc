package com.rongzhe.house.pay;

import java.util.Map;

/**
 * Created by hggxg on 2017/11/15.
 */
public interface PayService {

    String getSign(Map<String, String> map, String rsaKey, boolean rsa2);

    AliPayPost checkSign(Map<String, String> paramMap);

}