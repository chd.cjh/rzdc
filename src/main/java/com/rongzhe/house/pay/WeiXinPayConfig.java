package com.rongzhe.house.pay;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by hggxg on 2017/11/15.
 */
@Component
public class WeiXinPayConfig {
    @Value("${pay.wx.appid}")
    private String appid;
    @Value("${pay.wx.mch_id}")
    private String mch_id;
    @Value("${pay.wx.notify_url}")
    private String notify_url;
    @Value("${pay.wx.key}")
    private String key;
    @Value("${pay.wx.trade_type}")
    private String trade_type;
    @Value("${pay.wx.precreate_Url}")
    private String precreate_Url;
    @Value("${pay.wx.appsecret}")
    private String appsecret;
    @Value("${pay.wx.redirect_uri}")
    private String redirectUrl;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getPrecreate_Url() {
        return precreate_Url;
    }

    public void setPrecreate_Url(String precreate_Url) {
        this.precreate_Url = precreate_Url;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
