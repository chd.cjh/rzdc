package com.rongzhe.house.user.service;

import com.rongzhe.house.user.entity.UserRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hegang
 * @since 2018-04-14
 */
public interface UserRoleService extends IService<UserRole> {

}
