package com.rongzhe.house.user.service;

import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.user.entity.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by JackWangon[www.aiprogram.top] 2017/7/29.
 */
public interface UserService {

    String login(LoginInfo loginInfo) throws RongZheBusinessException;

    String uploadHeadImg(MultipartFile file, Integer userId) throws RongZheBusinessException;

    void sendVercode(String mobile, String ip) throws RongZheBusinessException;

    UserBase findById(Integer userId) throws RongZheBusinessException;

    void modifyNickName(UserBase user) throws RongZheBusinessException;

    UserIdentifyCardInfo addIdentifyCard(UserIdentifyCardInfo userIdentifyCardInfo);

    UserIdentifyCardInfo getIdentifyCardInfoByUid(Integer uid);

    UserComplaintSuggest complaintSuggest(UserComplaintSuggest userComplaintSuggest);

    List<UserComplaintSuggest> getComplaintSuggest(Integer currentPage, Integer pageSize, Integer uid);


    List<UserContract> contractList(Integer currentPage, Integer pageSize, String Uid);

    UserContract contractDetail(Integer contractId);

    /**
     * 查询用户的签约房源
     * @param uid
     * @return
     */
    List<UserHouse> queryUserHouse(String uid);

    /**
     * 确认合同
     * @param id
     */
    void confirmContract(Integer id);
}
