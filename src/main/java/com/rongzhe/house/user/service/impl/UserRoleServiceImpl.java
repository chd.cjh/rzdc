package com.rongzhe.house.user.service.impl;

import com.rongzhe.house.user.entity.UserRole;
import com.rongzhe.house.user.mapper.UserRoleMapper;
import com.rongzhe.house.user.service.UserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hegang
 * @since 2018-04-14
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
