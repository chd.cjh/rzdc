package com.rongzhe.house.user.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.user.dao.UserHouseAppointmentMapper;
import com.rongzhe.house.user.entity.UserHouseAppointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by hggxg on 2018/1/1.
 */
@Service
public class UserHouseAppointmentServiceImpl implements UserHouseAppointmentService {

    @Autowired
    private UserHouseAppointmentMapper userHouseAppointmentMapper;

    @Override
    public UserHouseAppointment appointment(UserHouseAppointment userHouseAppointment) throws RongZheBusinessException {


        //魔数 TODO
        userHouseAppointment.setState(1);
        userHouseAppointment.setInsertTime(new Date());
        userHouseAppointmentMapper.insertSelective(userHouseAppointment);

        return userHouseAppointment;
    }

    @Override
    public List<UserHouseAppointment> getAppointmentByUid(Integer uid, Integer pageSize, Integer currentPage, Integer status) throws RongZheBusinessException {

        PageHelper.startPage(currentPage, pageSize);

        PageHelper.orderBy("uha.insert_time DESC");

        PageInfo pageInfo = new PageInfo(userHouseAppointmentMapper.selectByUid(uid, status));

        return pageInfo.getList();
    }
}
