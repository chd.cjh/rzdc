package com.rongzhe.house.user.service;

import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.user.entity.UserHouseAppointment;

import java.util.List;

/**
 * Created by hggxg on 2018/1/1.
 */
public interface UserHouseAppointmentService {

    UserHouseAppointment appointment(UserHouseAppointment userHouseAppointment) throws RongZheBusinessException;

    List<UserHouseAppointment> getAppointmentByUid(Integer uid, Integer pageSize, Integer currentPage, Integer status) throws RongZheBusinessException;


}
