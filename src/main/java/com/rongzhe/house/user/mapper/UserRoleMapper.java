package com.rongzhe.house.user.mapper;

import com.rongzhe.house.user.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hegang
 * @since 2018-04-14
 */
@Repository
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
