package com.rongzhe.house.user.dao;

import com.rongzhe.house.user.entity.UserComplaintSuggest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserComplaintSuggestMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserComplaintSuggest record);

    int insertSelective(UserComplaintSuggest record);

    UserComplaintSuggest selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserComplaintSuggest record);

    int updateByPrimaryKey(UserComplaintSuggest record);

    List<UserComplaintSuggest> selectList(@Param("uid") Integer uid);
}