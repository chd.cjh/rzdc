package com.rongzhe.house.user.dao;

import com.rongzhe.house.user.entity.UserIdentifyCardInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserIdentifyCardInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserIdentifyCardInfo record);

    int insertSelective(UserIdentifyCardInfo record);

    UserIdentifyCardInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserIdentifyCardInfo record);

    int updateByPrimaryKey(UserIdentifyCardInfo record);

    UserIdentifyCardInfo selectByUid(Integer uid);

    UserIdentifyCardInfo selectByUidForUpdate(Integer uid);
}