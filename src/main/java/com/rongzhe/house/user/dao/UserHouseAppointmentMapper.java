package com.rongzhe.house.user.dao;

import com.rongzhe.house.user.entity.UserHouseAppointment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserHouseAppointmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserHouseAppointment record);

    int insertSelective(UserHouseAppointment record);

    UserHouseAppointment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserHouseAppointment record);

    int updateByPrimaryKey(UserHouseAppointment record);

    List<UserHouseAppointment> selectByUid(@Param("uid") Integer uid, @Param("state") Integer state);
}