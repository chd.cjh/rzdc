package com.rongzhe.house.user.dao;

import com.rongzhe.house.order.entity.RentInfo;
import com.rongzhe.house.user.entity.UserBase;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserBaseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserBase record);

    int insertSelective(UserBase record);

    UserBase selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserBase record);

    int updateByPrimaryKey(UserBase record);


    /**
     * 根据手机号码查找用户
     *
     * @param mobile
     * @return
     */
    UserBase selectByMobile(String mobile);

    UserBase selectDetailByPrimaryKey(Integer id);

    /**
     * 用于生成合同
     * 查询用户信息
     * @param userId 用户id
     */
    RentInfo selectRentInfoByUserId(@Param("id") Integer userId);

}