package com.rongzhe.house.user.dao;

import com.rongzhe.house.user.entity.UserContract;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserContractMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserContract record);

    int insertSelective(UserContract record);

    UserContract selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserContract record);

    int updateByPrimaryKey(UserContract record);

    UserContract selectByOrderCode(@Param("orderCode") String code);

    List selectListByUid(@Param("uid") String uid);

    List queryUserHouse (@Param("uid") String uid);

    /**
     * 通过用户id 查询 有效的合同
     * @param currentUserId
     * @return
     */
    Integer selectIdByUserId(@Param("uid")Integer currentUserId);


    /**
     * 通过id 查询状态
     * @param id
     * @return
     */
    Integer selectStatusById(@Param("id")Integer id);

    /**
     * 更改合同状态
     * @param id
     */
    void updateConfirmContract(Integer id);
}