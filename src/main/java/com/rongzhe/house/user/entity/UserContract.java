package com.rongzhe.house.user.entity;

import com.rongzhe.house.house.entity.HouseOverview;
import com.rongzhe.house.order.entity.OrderSnapshot;
import com.rongzhe.house.order.entity.OrderSnapshotContent;
import lombok.Data;

import java.io.PipedReader;
import java.util.Date;

@Data
public class UserContract {
    private Integer id;

    private Integer uid;

    private String orderCode;

    private Integer houseId;

    private Date startTime;

    private Date endTime;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;


    private String realName;

    private String identifyCardNum;

    private String snapshotStr;

    private OrderSnapshot orderSnapshot;


    private OrderSnapshotContent snapshotContent;

    private HouseOverview houseOverview;


    /**
     * 退租申请的状态
     */
    private String applyStatus;

    /**
     * 合同状态
     */
    private String contractStatus;

    /**
     * 是否电子合同
     */
    private Integer eleContract;

    /**
     * 合同图片
     */
    private String contractUrl;

    /**
     * 是否确认
     */
    private String confirm;
}