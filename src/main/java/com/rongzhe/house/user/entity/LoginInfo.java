package com.rongzhe.house.user.entity;

import lombok.Data;

/**
 * Created by JackWangon[www.coder520.com] 2017/8/2.
 */
@Data
public class LoginInfo {

    private String data;

    private String key;

    private String mobile;

    private String code;

    private String platform;

    private String channelId;

    private String device;

}
