package com.rongzhe.house.user.entity;

import com.rongzhe.house.house.entity.HouseOverview;
import lombok.Data;

import java.util.Date;

@Data
public class UserHouseAppointment {

    private Integer id;

    private Integer houseId;

    private String contacts;

    private String contactsMobile;

    private Integer contactsSex;

    private Date contactTime;

    private String message;

    private String contactTimeOption;

    private Integer handleUid;

    private Integer allocationUid;

    private Integer state;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private HouseOverview houseOverview;

    private UserBase allocationUser;

}