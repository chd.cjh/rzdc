package com.rongzhe.house.user.entity;

import lombok.Data;

import java.util.Date;

@Data
public class UserBase {

    private Integer id;

    private String realname;

    private String constellation;

    private String profession;

    private String mobile;

    private String password;

    private String token;

    private String sex;

    private Byte status;

    private String avatar;

    private String nickname;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private Integer identityState;

    private String introduce;

    private Date lastLoginTime;

    private String lastDevice;

}