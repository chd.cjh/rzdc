package com.rongzhe.house.user.entity;

import lombok.Data;

import java.util.Date;

@Data
public class UserComplaintSuggest {
    private Integer id;

    private Integer uid;

    private String type;

    private String bizType;

    private String bizContent;

    private String promiseService;

    private String resolveContent;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private UserBase user;

}