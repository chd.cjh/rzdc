package com.rongzhe.house.user.entity;

import lombok.Data;

import java.util.Date;

@Data
public class UserIdentifyCardInfo {
    private Integer id;

    private Integer uid;

    private String identifyCardImageFront;

    private String identifyCardImageBack;

    private Integer checkUid;

    private String remark;

    private String state;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private String realName;

    private String identifyCardNum;

    private String mobile;

    private String sex;

}