package com.rongzhe.house.user.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hegang
 * @since 2018-04-14
 */
@Data
@Accessors(chain = true)
@TableName("user_role")
public class UserRole extends Model<UserRole> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    @TableField("role_code")
    private String roleCode;
    @TableField("insert_time")
    private Date insertTime;
    @TableField("insert_uid")
    private Integer insertUid;
    @TableField("update_time")
    private Date updateTime;
    @TableField("update_uid")
    private Integer updateUid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
