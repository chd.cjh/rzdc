package com.rongzhe.house.user.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户签订的房源
 * @author Chengjia
 * @date 2018/5/14 9:09
 */
@Data
public class UserHouse {

    /**
     * 房源ID
     */
    private Integer houseId;

    /**
     * 房源名称
     */
    private String houseName;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 租客电话
     */
    private String mobile;

    /**
     * 栋
     */
    private String buildingNum;

    /**
     * 单元数
     */
    private String houseUnit;

    /**
     * 门牌号
     */
    private String houseNum;

    /**
     * 租金
     */
    private BigDecimal rent;

    /**
     * 小区名称
     */
    private String communityName;

    /**
     * 封面图
     */
    private String cover;


}
