package com.rongzhe.house.plus;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>Description: 数据处理  </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author yangpeng
 * @version 1.0
 * @date 2018/1/6
 * @since 1.8
 */
@Component
@Slf4j
public class MyMetaObjectHandler extends MetaObjectHandler {

    /**
     * 更新时间
     */
    public static final String UPDATE_TIME = "updateTime";
    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "createTime";

    /**
     * 创建者id
     */
    public static final String UPDATE_ID = "updateId";
    /**
     * 更新者id
     */
    public static final String CREATE_ID = "createId";

    //新增填充
    @Override
    public void insertFill(MetaObject metaObject) {
        log.debug("insert set createTime createBy updateTime updateBy param");
//        insertAction(metaObject);
//        updateAction(metaObject);
    }

    //更新填充
    @Override
    public void updateFill(MetaObject metaObject) {
        log.debug("update set  updateTime updateBy param");
//        updateAction(metaObject);
    }

    /**
     * 插入数据
     *
     * @param metaObject 元数据
     */
    private void insertAction(MetaObject metaObject) {
        Object createId = getFieldValByName(CREATE_ID, metaObject);
        Object createTime = getFieldValByName(CREATE_TIME, metaObject);
        if (null == createId) {
            setFieldValByName(CREATE_ID, 0L, metaObject);
        }
        if (null == createTime) {
            setFieldValByName(CREATE_TIME, new Date(), metaObject);
        }
    }

    /**
     * 更新操作
     *
     * @param metaObject 元数据
     */
    private void updateAction(MetaObject metaObject) {
        Object updateId = getFieldValByName(UPDATE_ID, metaObject);
        Object updateTime = getFieldValByName(UPDATE_TIME, metaObject);

        if (null == updateId) {
            setFieldValByName(UPDATE_ID, 0L, metaObject);
        }
        if (null == updateTime) {
            setFieldValByName(UPDATE_TIME, new Date(), metaObject);
        }
    }
}
