package com.rongzhe.house.contract.mapper;

import com.rongzhe.house.contract.entity.SysVersion;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * app版本 Mapper 接口
 * </p>
 *
 * @author hegang
 * @since 2018-04-04
 */
@Mapper
@Repository
public interface SysVersionMapper extends BaseMapper<SysVersion> {

}
