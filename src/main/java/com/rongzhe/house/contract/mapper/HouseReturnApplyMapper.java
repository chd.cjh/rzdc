package com.rongzhe.house.contract.mapper;

import com.rongzhe.house.contract.entity.HouseReturnApply;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.rongzhe.house.house.vo.HouseReturnApplyVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hegang
 * @since 2018-03-20
 */
@Repository
@Mapper
public interface HouseReturnApplyMapper extends BaseMapper<HouseReturnApply> {

    HouseReturnApplyVO selectDetailByContractId(@Param("id") Integer id);
}
