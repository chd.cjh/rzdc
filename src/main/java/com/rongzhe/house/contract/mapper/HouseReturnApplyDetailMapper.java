package com.rongzhe.house.contract.mapper;

import com.rongzhe.house.contract.entity.HouseReturnApplyDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 退租详情 Mapper 接口
 * </p>
 *
 * @author yangpeng123
 * @since 2018-03-20
 */
@Repository
@Mapper
public interface HouseReturnApplyDetailMapper extends BaseMapper<HouseReturnApplyDetail> {

}
