package com.rongzhe.house.contract.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 退租详情
 * </p>
 *
 * @author yangpeng123
 * @since 2018-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("house_return_apply_detail")
public class HouseReturnApplyDetail extends Model<HouseReturnApplyDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * v
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 内容
     */
    private String content;
    /**
     * 退租申请表id
     */
    @TableField("house_return_apply_id")
    private Integer houseReturnApplyId;
    /**
     * 退租类型 数据字典 rent_state_optons
     */
    @TableField("return_state")
    private String returnState;
    /**
     * 应退押金
     */
    @TableField("return_deposit")
    private BigDecimal returnDeposit;
    /**
     * 应退租金
     */
    @TableField("return_rent")
    private BigDecimal returnRent;
    /**
     * 扣除费用
     */
    @TableField("return_deduction")
    private BigDecimal returnDeduction;
    /**
     * 余额总计
     */
    @TableField("total_price")
    private BigDecimal totalPrice;
    /**
     * 备注
     */
    private String remark;
    @TableField("insert_uid")
    private Integer insertUid;
    @TableField("insert_time")
    private Date insertTime;
    @TableField("update_uid")
    private Integer updateUid;
    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
