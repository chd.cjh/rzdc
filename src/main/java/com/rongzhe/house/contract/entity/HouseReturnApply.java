package com.rongzhe.house.contract.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hegang
 * @since 2018-03-20
 */
@Data
@Accessors(chain = true)
@TableName("house_return_apply")
public class HouseReturnApply extends Model<HouseReturnApply> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    @TableField("contract_id")
    private Integer contractId;
    /**
     * 管理人id
     */
    @TableField("manager_id")
    private Integer managerId;
    @TableField("account_num")
    private String accountNum;
    @TableField("account_location")
    private String accountLocation;

    /**
     * 来源
     */
    private String source;

    /**
     * 备注
     */
    private String remark;
    /**
     * 退租原因
     */
    private String reason;
    /**
     * 结果
     */
    private String result;
    /**
     * 1 待处理 2 审核中 3 通过 4 确认 5 取消 6 打款中 7 已完成
     */
    private String status;
    /**
     * 插入时间
     */
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 插入uid
     */
    @TableField("insert_uid")
    private Integer insertUid;
    /**
     * 更改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 更新人uid
     */
    @TableField("update_uid")
    private Integer updateUid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
