package com.rongzhe.house.contract.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * app版本
 * </p>
 *
 * @author hegang
 * @since 2018-04-04
 */
@Data
@Accessors(chain = true)
@TableName("sys_version")
public class SysVersion extends Model<SysVersion> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 名称 数据字典
     */
    private String name;
    /**
     * 渠道
     */
    private String channel;
    /**
     * 类型PC IOS Anroid
     */
    private String type;
    /**
     * 版本号
     */
    private String version;
    /**
     * 创建时间
     */
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 创建人
     */
    @TableField("insert_id")
    private Integer insertId;
    /**
     * 更新内容
     */
    private String content;
    /**
     * 状态 0 正在上线 1 成功上线
     */
    @TableField("current_state")
    private Integer currentState;
    /**
     * 扩展字段
     */
    private String ext;

    @TableField(exist = false)
    private Map param;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
