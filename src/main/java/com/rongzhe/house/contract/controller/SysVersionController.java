package com.rongzhe.house.contract.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * app版本 前端控制器
 * </p>
 *
 * @author hegang
 * @since 2018-04-04
 */
@RestController
@RequestMapping("/contract/sysVersion")
public class SysVersionController {

}

