package com.rongzhe.house.contract.service;

import com.rongzhe.house.contract.entity.SysVersion;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * app版本 服务类
 * </p>
 *
 * @author hegang
 * @since 2018-04-04
 */

public interface SysVersionService extends IService<SysVersion> {

}
