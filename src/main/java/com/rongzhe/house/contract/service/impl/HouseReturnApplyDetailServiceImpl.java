package com.rongzhe.house.contract.service.impl;

import com.rongzhe.house.contract.entity.HouseReturnApplyDetail;
import com.rongzhe.house.contract.mapper.HouseReturnApplyDetailMapper;
import com.rongzhe.house.contract.service.HouseReturnApplyDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退租详情 服务实现类
 * </p>
 *
 * @author yangpeng123
 * @since 2018-03-20
 */
@Service
public class HouseReturnApplyDetailServiceImpl extends ServiceImpl<HouseReturnApplyDetailMapper, HouseReturnApplyDetail> implements HouseReturnApplyDetailService {

}
