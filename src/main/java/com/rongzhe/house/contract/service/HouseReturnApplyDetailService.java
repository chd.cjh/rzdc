package com.rongzhe.house.contract.service;

import com.rongzhe.house.contract.entity.HouseReturnApplyDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 退租详情 服务类
 * </p>
 *
 * @author yangpeng123
 * @since 2018-03-20
 */
public interface HouseReturnApplyDetailService extends IService<HouseReturnApplyDetail> {

}
