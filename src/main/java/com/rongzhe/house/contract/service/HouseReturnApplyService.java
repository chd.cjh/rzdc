package com.rongzhe.house.contract.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.contract.entity.HouseReturnApply;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hegang
 * @since 2018-03-20
 */
public interface HouseReturnApplyService extends IService<HouseReturnApply> {

    ApiResult selectByContractId(Integer id);


    /**
     * 添加退租
     * @param houseReturnApply
     */
    ApiResult insertRecord(HouseReturnApply houseReturnApply);


}
