package com.rongzhe.house.contract.service.impl;

import com.rongzhe.house.contract.entity.SysVersion;
import com.rongzhe.house.contract.mapper.SysVersionMapper;
import com.rongzhe.house.contract.service.SysVersionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * app版本 服务实现类
 * </p>
 *
 * @author hegang
 * @since 2018-04-04
 */
@Service
public class SysVersionServiceImpl extends ServiceImpl<SysVersionMapper, SysVersion> implements SysVersionService {

}
