package com.rongzhe.house.contract.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.utils.RequestUtil;
import com.rongzhe.house.contract.entity.HouseReturnApply;
import com.rongzhe.house.contract.mapper.HouseReturnApplyMapper;
import com.rongzhe.house.contract.service.HouseReturnApplyService;
import com.rongzhe.house.house.vo.HouseReturnApplyVO;
import com.rongzhe.house.user.dao.UserContractMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hegang
 * @since 2018-03-20
 */
@Service
public class HouseReturnApplyServiceImpl extends ServiceImpl<HouseReturnApplyMapper, HouseReturnApply> implements HouseReturnApplyService {


    @Autowired
    private HouseReturnApplyMapper houseReturnApplyMapper;

    @Autowired
    private UserContractMapper userContractMapper;


    @Autowired
    private RequestUtil requestUtil;

    @Override
    public ApiResult selectByContractId(Integer id) {

        baseMapper.selectById(1);

        HouseReturnApplyVO houseReturnApplyVO = houseReturnApplyMapper.selectDetailByContractId(id);
        houseReturnApplyVO.setSnapshot(houseReturnApplyVO.getSnapshots());

        houseReturnApplyVO.setSnapshots(null);

        return new ApiResult(houseReturnApplyVO);
    }

    @Override
    public ApiResult insertRecord(HouseReturnApply houseReturnApply) {
        try {
            Integer currentUserId = requestUtil.getCurrentUserId();
            if(currentUserId == null){
                return ApiResult.buildUnloginFail();
            }
            Integer contractId = houseReturnApply.getContractId();
            if(contractId == null){
                return ApiResult.buildFail("当前没有有效的合同");
            }
            Integer integer = userContractMapper.selectStatusById(contractId);
            if(null == integer || integer!=1){
                return ApiResult.buildFail("当前没有有效的合同");
            }
            Integer count = houseReturnApplyMapper.selectCount(Condition.create().eq("contract_id", contractId).eq("status", 1));
            if(count.intValue()>0){
                return ApiResult.buildOk("已提交过申请，无须重复申请");
            }
            houseReturnApply.setContractId(contractId);
            // 1 申请中
            houseReturnApply.setStatus("1");
            houseReturnApply.setUid(currentUserId);

            Date date = new Date();
            houseReturnApply.setInsertUid(currentUserId);
            houseReturnApply.setInsertTime(date);
            houseReturnApply.setUpdateTime(date);
            houseReturnApply.setUpdateUid(currentUserId);

            houseReturnApplyMapper.insert(houseReturnApply);
            return ApiResult.buildOk("提交申请成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ApiResult.buildOk("提交申请成功");
    }


}
