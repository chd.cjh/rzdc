package com.rongzhe.house.message.web;


import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.rest.BaseController;
import com.rongzhe.house.house.service.HouseService;
import com.rongzhe.house.message.criteria.HouseMsgCriteria;
import com.rongzhe.house.message.entity.HouseMsg;
import com.rongzhe.house.message.service.HouseMsgService;
import com.rongzhe.house.user.entity.UserElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 消息 前端控制器
 * </p>
 *
 * @author HeGang
 * @since 2018-05-03
 */
@RestController
@RequestMapping("/message")
public class HouseMsgController extends BaseController {


    @Autowired
    private HouseMsgService houseMsgService;


    @RequestMapping("/unReadCount")
    public ApiResult unReadCount(@RequestBody HouseMsgCriteria criteria) {

        return houseMsgService.unReadCount(criteria);
    }


    @RequestMapping("/list")
    public ApiResult list(@RequestBody HouseMsgCriteria criteria) {

        UserElement userElement = getCurrentUser();

        criteria.setResponseId(userElement.getUserId());

        return houseMsgService.list(criteria);
    }


    @RequestMapping("/read")
    public ApiResult read(@RequestBody HouseMsg houseMsg) {

        UserElement userElement = getCurrentUser();
        houseMsg.setResponseId(userElement.getUserId());

        return houseMsgService.read(houseMsg);
    }

    @RequestMapping("/readAll")
    public ApiResult readAll() {

        UserElement userElement = getCurrentUser();

        return houseMsgService.readAll(userElement.getUserId());
    }

}

