package com.rongzhe.house.message.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import lombok.Data;

/**
 * Created by JackWangon[www.coder520.com] 16:35 2018/5/3.
 */
@Data
public class HouseMsgCriteria extends BaseParam {

    private Integer responseId;
}
