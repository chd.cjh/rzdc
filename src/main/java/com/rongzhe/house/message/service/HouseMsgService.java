package com.rongzhe.house.message.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.message.criteria.HouseMsgCriteria;
import com.rongzhe.house.message.entity.HouseMsg;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息 服务类
 * </p>
 *
 * @author HeGang
 * @since 2018-05-03
 */
@Service
public interface HouseMsgService extends IService<HouseMsg> {


    ApiResult unReadCount(HouseMsgCriteria criteria);

    ApiResult list(HouseMsgCriteria criteria);

    ApiResult read(HouseMsg houseMsg);

    ApiResult readAll(Integer userId);
}
