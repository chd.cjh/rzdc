package com.rongzhe.house.message.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.utils.PageUtils;
import com.rongzhe.house.message.criteria.HouseMsgCriteria;
import com.rongzhe.house.message.entity.HouseMsg;
import com.rongzhe.house.message.mapper.HouseMsgMapper;
import com.rongzhe.house.message.service.HouseMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 消息 服务实现类
 * </p>
 *
 * @author HeGang
 * @since 2018-05-03
 */
@Service
public class HouseMsgServiceImpl extends ServiceImpl<HouseMsgMapper, HouseMsg> implements HouseMsgService {


    @Autowired
    private HouseMsgMapper houseMsgMapper;

    @Override
    public ApiResult unReadCount(HouseMsgCriteria criteria) {


        int count = houseMsgMapper.selectCount(new EntityWrapper<HouseMsg>()
                .eq("response_id", criteria.getResponseId())
                .eq("is_read", 0)
        );
        return new ApiResult(count);
    }

    @Override
    public ApiResult list(HouseMsgCriteria criteria) {
        PageUtils.initPageParam(criteria);
        List<HouseMsg> list = houseMsgMapper.list(criteria);
        return new ApiResult(list);


    }

    @Override
    public ApiResult read(HouseMsg houseMsg) {


        HouseMsg forUpdate = new HouseMsg();

        forUpdate.setIsRead(1);

        EntityWrapper<HouseMsg> houseMsgEntityWrapper = new EntityWrapper<>();

        houseMsgEntityWrapper.eq("responseId", houseMsg.getResponseId())
                .eq("id", houseMsg.getId());


        update(houseMsg, houseMsgEntityWrapper);

        return new ApiResult();
    }

    @Override
    public ApiResult readAll(Integer userId) {


        HouseMsg houseMsg = new HouseMsg();

        houseMsg.setIsRead(1);

        EntityWrapper<HouseMsg> houseMsgEntityWrapper = new EntityWrapper<>();

        houseMsgEntityWrapper.eq("responseId", userId);

        update(houseMsg, houseMsgEntityWrapper);


        return new ApiResult();
    }
}
