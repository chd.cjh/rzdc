package com.rongzhe.house.message.mapper;

import com.rongzhe.house.message.criteria.HouseMsgCriteria;
import com.rongzhe.house.message.entity.HouseMsg;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 消息 Mapper 接口
 * </p>
 *
 * @author HeGang
 * @since 2018-05-03
 */
@Mapper
@Repository
public interface HouseMsgMapper extends BaseMapper<HouseMsg> {


    List<HouseMsg> list(HouseMsgCriteria criteria);

}
