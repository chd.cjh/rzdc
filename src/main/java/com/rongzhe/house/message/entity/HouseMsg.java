package com.rongzhe.house.message.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息
 * </p>
 *
 * @author HeGang
 * @since 2018-05-03
 */
@Data
@Accessors(chain = true)
@TableName("house_msg")
public class HouseMsg extends Model<HouseMsg> {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 类型 1 拿房 2 出房
     */
    @TableField("biz_type")
    private String bizType;
    /**
     * id 标示
     */
    @TableField("biz_id")
    private Integer bizId;
    /**
     * 简要
     */
    private String summar;
    /**
     * 详细内容
     */
    private String content;
    /**
     * 发送人id
     */
    @TableField("request_id")
    private Integer requestId;
    /**
     * 相应人id
     */
    @TableField("response_id")
    private Integer responseId;
    /**
     * 扩展字段
     */
    private String ext;
    /**
     * 是否已读 1 已读 0 未读
     */
    @TableField("is_read")
    private Integer isRead;
    /**
     * 链接 审核 系统
     */
    private String type;
    /**
     * 创建时间
     */
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    @TableField(exist = false)
    private String requestName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
