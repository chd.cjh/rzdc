package com.rongzhe.house.order.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hggxg on 2018/1/21.
 */
public class ResponseCode {


    public static final int PAID = 600001;

    public static final int ALI_PAY_CALLBACK_CHECK_FAIL = 600002;

    private static final Map<Integer, String> map = new HashMap<Integer, String>() {
        {
            put(PAID, "订单已支付");
            put(ALI_PAY_CALLBACK_CHECK_FAIL, "非法请求");
        }
    };

    public static String getMessage(int code) {

        return map.get(code);
    }

}
