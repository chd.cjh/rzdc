package com.rongzhe.house.order.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.order.criteria.ItemCriteria;
import com.rongzhe.house.order.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hggxg on 2018/1/21.
 */
@RestController
@RequestMapping("item")
public class ItemController {


    @Autowired
    private ItemService itemService;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ApiResult getItem(@RequestParam("houseId") Integer houseId) {

        ItemCriteria itemCriteria = new ItemCriteria();
        itemCriteria.setBizId(houseId);
        //TODO 魔数
        itemCriteria.setBizType("1");

        return itemService.list(itemCriteria);

    }
}
