package com.rongzhe.house.order.dao;

import com.rongzhe.house.order.criteria.OrderCriteria;
import com.rongzhe.house.order.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);


    Order selectByCodeForUpdate(String orderCode);

    void updateByCode(Order orderUpdate);

    List<Order> selectByCriteria(OrderCriteria orderCriteria);

    Order detailByOrderCode(@Param("code") String orderCode);
}