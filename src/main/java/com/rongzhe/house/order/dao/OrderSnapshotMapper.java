package com.rongzhe.house.order.dao;

import com.rongzhe.house.order.entity.OrderSnapshot;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface OrderSnapshotMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderSnapshot record);

    int insertSelective(OrderSnapshot record);

    OrderSnapshot selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderSnapshot record);

    int updateByPrimaryKey(OrderSnapshot record);
}