package com.rongzhe.house.order.dao;

import com.rongzhe.house.order.entity.ItemProperty;

public interface ItemPropertyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ItemProperty record);

    int insertSelective(ItemProperty record);

    ItemProperty selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ItemProperty record);

    int updateByPrimaryKey(ItemProperty record);
}