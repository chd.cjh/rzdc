package com.rongzhe.house.order.dao;

import com.rongzhe.house.order.criteria.ItemCriteria;
import com.rongzhe.house.order.entity.Item;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface ItemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Item record);

    int insertSelective(Item record);

    Item selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Item record);

    int updateByPrimaryKey(Item record);

    List<Item> selectByItemCriteria(ItemCriteria itemCriteria);

    Item selectByPrimaryKeyForUpdate(Integer itemId);

    List<Item> selectByHouseId(Integer houseId);
}