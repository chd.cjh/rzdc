package com.rongzhe.house.order.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.rongzhe.house.order.entity.OrderBillInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 账单订单信息表 Mapper 接口
 * </p>
 *
 * @author ChengJIa
 * @since 2018-05-26
 */
@Repository
@Mapper
public interface OrderBillInfoMapper extends BaseMapper<OrderBillInfo> {

}
