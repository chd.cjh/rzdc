package com.rongzhe.house.order.enums;

/**
 * Created by hggxg on 2018/1/21.
 */
public enum OrderStatus {


    //1 下单(等待支付)  2 已支付（支付完成） 3 订单超时  4 无效


    WAIT_PAY((byte) 1), PAID((byte) 2);


    private byte status;

    OrderStatus(byte status) {
        this.status = status;
    }

    public byte getStatus() {
        return status;
    }
}
