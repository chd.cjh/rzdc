package com.rongzhe.house.order.enums;

/**
 * Created by hggxg on 2017/11/19.
 */
public enum PayType {

    WX(1), ALI(2);
    private int type;

    PayType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public boolean isAliPay() {
        if (this.getType() == PayType.ALI.getType()) {
            return true;
        }
        return false;
    }

    public boolean isWx() {
        if (this.getType() == PayType.WX.getType()) {
            return true;
        }
        return false;
    }

    public static PayType getPayTypeByValue(Integer value) {

        for (PayType payType : PayType.values()) {
            if (payType.getType() == value) {
                return payType;
            }
        }

        return null;
    }


}
