package com.rongzhe.house.order.enums;

public enum ItemBizType {


    HOUSE_FIRST_PAY("1"),
    HOUSE_RENT_PAY("2");

    private String type;

    ItemBizType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
