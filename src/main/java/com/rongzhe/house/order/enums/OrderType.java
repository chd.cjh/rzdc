package com.rongzhe.house.order.enums;

/**
 * Created by JackWangon[www.coder520.com] 17:59 2018/4/2.
 */
public enum OrderType {
    HouseRent("1"), Bill("2");

    private String type;

    OrderType(String type) {
        this.type = type;
    }


    public String getType() {
        return type;
    }
}
