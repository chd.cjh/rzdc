package com.rongzhe.house.order.entity;

import com.rongzhe.house.user.entity.UserContract;
import com.rongzhe.house.user.entity.UserIdentifyCardInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {
    private Integer id;

    private Byte status;

    private String code;

    private Date createTime;

    private Date paidTime;

    private String type;

    private BigDecimal amount;

    private BigDecimal payableAmount;

    private String remark;

    private Integer bizId;

    private Integer createUid;

    private Integer paidUid;

    private Byte payment;

    private String outTradeNo;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private Byte deleted;

    private String snapshot;

    private OrderSnapshotContent content;

    private UserContract userContract;

    private UserIdentifyCardInfo userIdentifyCardInfo;

}