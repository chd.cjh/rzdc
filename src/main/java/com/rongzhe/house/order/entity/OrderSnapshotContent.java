package com.rongzhe.house.order.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by hggxg on 2018/1/21.
 */
@Data
public class OrderSnapshotContent {

    private String cover;

    private String name;

    private BigDecimal price;

    private String houseId;

    private String regionName;

    private String regionFinalName;

    private String rentPayment;

    private BigDecimal pledge;

    private BigDecimal serviceFee;

    private String roomNum;

    private String orientation;

}
