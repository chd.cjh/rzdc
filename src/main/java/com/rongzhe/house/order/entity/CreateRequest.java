package com.rongzhe.house.order.entity;

import lombok.Data;

/**
 * Created by hggxg on 2017/11/19.
 */
@Data
public class CreateRequest {

    /**
     * 商品ID
     */
    private Integer itemId;

    //默认 1个
    private Integer count = 1;

    /**
     * 账单ID
     *
     */
    private Integer billId;
}
