package com.rongzhe.house.order.entity;

import lombok.Data;

/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/10
 */
@Data
public class RentInfo {

    private String mobile;

    private String sex;

    private Integer id;
    private String state;

    private String identifyCardNum;
    private String realName;

    private String identifyCardImageFront;
    private String identifyCardImageBack;
}
