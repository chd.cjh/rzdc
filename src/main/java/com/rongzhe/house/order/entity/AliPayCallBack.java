package com.rongzhe.house.order.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by hggxg on 2018/1/21.
 */
@Data
public class AliPayCallBack {


    private String trade_no;

    private String out_trade_no;

    private String trade_status;

    private String total_amount;

    private String receipt_amount;

    private Date gmt_payment;


}
