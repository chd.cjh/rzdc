package com.rongzhe.house.order.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 账单订单信息表
 * </p>
 *
 * @author ChengJIa
 * @since 2018-05-26
 */
@Data
@Accessors(chain = true)
@TableName("order_bill_info")
public class OrderBillInfo extends Model<OrderBillInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 订单code
     */
    @TableField("order_code")
    private String orderCode;
    /**
     * 账单ID
     */
    @TableField("bill_id")
    private Integer billId;
    /**
     * 创建时间
     */
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 创建人
     */
    @TableField("insert_uid")
    private Integer insertUid;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 修改人
     */
    @TableField("update_uid")
    private Integer updateUid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
