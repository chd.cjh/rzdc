package com.rongzhe.house.order.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ItemExt {

    private String rentOption;

    private BigDecimal pledge;

    private BigDecimal rent;

    private BigDecimal serviceFee;

}
