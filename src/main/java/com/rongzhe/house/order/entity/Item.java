package com.rongzhe.house.order.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Item {
    private Integer id;

    private String ext;

    private BigDecimal price;

    private String name;

    private String bizType;

    private Integer bizId;

    private Integer parentId;

    private Boolean show;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;
}