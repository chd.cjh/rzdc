package com.rongzhe.house.order.entity;

import lombok.Data;

/**
 * Created by hggxg on 2017/11/19.
 */
@Data
public class GetSignRequest {

    private String code;

    private Integer payType;

}
