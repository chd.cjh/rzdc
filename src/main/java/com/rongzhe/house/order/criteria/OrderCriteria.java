package com.rongzhe.house.order.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import lombok.Data;

/**
 * Created by hggxg on 2018/1/21.
 */
@Data
public class OrderCriteria extends BaseParam {

    private Integer uid;

    private Integer status;

    private String orderCode;
}
