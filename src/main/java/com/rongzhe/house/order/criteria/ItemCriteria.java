package com.rongzhe.house.order.criteria;

import lombok.Data;

/**
 * Created by hggxg on 2018/1/21.
 */
@Data
public class ItemCriteria {

    private Integer bizId;

    private String bizType;
}
