package com.rongzhe.house.order.service;

import com.rongzhe.house.order.entity.OrderBillInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 账单订单信息表 服务类
 * </p>
 *
 * @author ChengJIa
 * @since 2018-05-26
 */
public interface OrderBillInfoService extends IService<OrderBillInfo> {

}
