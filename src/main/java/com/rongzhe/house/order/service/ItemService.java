package com.rongzhe.house.order.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.order.criteria.ItemCriteria;

/**
 * Created by hggxg on 2018/1/21.
 */
public interface ItemService {


    ApiResult list(ItemCriteria itemCriteria);
}
