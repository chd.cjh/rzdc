package com.rongzhe.house.order.service;

import com.alipay.api.AlipayApiException;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.order.criteria.OrderCriteria;
import com.rongzhe.house.order.entity.CreateRequest;
import com.rongzhe.house.order.entity.Order;
import com.rongzhe.house.order.enums.PayType;
import com.rongzhe.house.pay.entity.WeiXinNotify;

import java.util.List;

/**
 * Created by hggxg on 2017/11/19.
 */

public interface OrderService {


    List<Order> list(OrderCriteria orderCriteria);


    Order detail(OrderCriteria orderCriteria);

    /**
     * @param itemId 商品
     * @param count  数量
     * @return
     */
    String createOrder(Integer itemId, Integer count, Integer uid) throws RongZheBusinessException;


    /**
     * 获取第三方签名
     *
     * @param orderCode
     * @param payType
     * @return
     */
    ApiResult getSign(String orderCode, PayType payType) throws AlipayApiException;


    /**
     * 微信回调
     * 确认订单
     *
     * @param weiXinNotify
     * @return
     */
    String wxCallback(WeiXinNotify weiXinNotify);

    /**
     * 主动查询
     *
     * @param orderCode
     * @return true 支付成功  false 支付失败
     */
    ApiResult query(String orderCode);

    /**
     * 回调支付成功
     *  @param out_trade_no
     * @param trade_no
     * @param type
     */
    void paid(String out_trade_no, String trade_no, int type);

    /**
     * 根据账单创建订单
     * @param billId
     * @param count
     * @param uid
     * @return
     * @throws RongZheBusinessException
     */
    String createCustomOrder(Integer billId, Integer count, Integer uid) throws RongZheBusinessException;


    /**
     * 根据账单创建订单
     * @param createRequests
     * @param uid
     * @return
     * @throws RongZheBusinessException
     */
    String createBillsOrder(List<CreateRequest> createRequests, Integer uid) throws RongZheBusinessException;

    /**
     * 展示合同
     * @param itemId
     * @param userId
     */
    ApiResult showContract(Integer itemId, Integer userId);
}
