package com.rongzhe.house.order.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rongzhe.house.order.dao.OrderBillInfoMapper;
import com.rongzhe.house.order.entity.OrderBillInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账单订单信息表 服务实现类
 * </p>
 *
 * @author ChengJIa
 * @since 2018-05-26
 */
@Service
public class OrderBillInfoServiceImpl extends ServiceImpl<OrderBillInfoMapper, OrderBillInfo> implements OrderBillInfoService {

}
