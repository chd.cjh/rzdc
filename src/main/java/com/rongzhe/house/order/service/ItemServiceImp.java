package com.rongzhe.house.order.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.order.criteria.ItemCriteria;
import com.rongzhe.house.order.dao.ItemMapper;
import com.rongzhe.house.order.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hggxg on 2018/1/21.
 */
@Service
public class ItemServiceImp implements ItemService {

    @Autowired
    private ItemMapper itemMapper;

    @Override
    public ApiResult list(ItemCriteria itemCriteria) {

        List<Item> itemList = itemMapper.selectByItemCriteria(itemCriteria);

        return new ApiResult(itemList);
    }
}
