package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.UserCollection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserCollectionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserCollection record);

    int insertSelective(UserCollection record);

    UserCollection selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserCollection record);

    int updateByPrimaryKey(UserCollection record);

    UserCollection selectByUidAndTypeAndBizIdForUpdate(@Param("uid") Integer userId, @Param("type") int type, @Param("bizId") Integer houseId);
}