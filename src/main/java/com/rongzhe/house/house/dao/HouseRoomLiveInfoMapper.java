package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseRoomLiveInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HouseRoomLiveInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseRoomLiveInfo record);

    int insertSelective(HouseRoomLiveInfo record);

    HouseRoomLiveInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseRoomLiveInfo record);

    int updateByPrimaryKey(HouseRoomLiveInfo record);

    List<HouseRoomLiveInfo> selectRoomInfoListByHouseId(Integer houseId);

    HouseRoomLiveInfo selectByHouseId(@Param("houseId") Integer houseId);
}