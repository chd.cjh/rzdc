package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseDelegationApply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface HouseDelegationApplyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseDelegationApply record);

    int insertSelective(HouseDelegationApply record);

    HouseDelegationApply selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseDelegationApply record);

    int updateByPrimaryKey(HouseDelegationApply record);
}