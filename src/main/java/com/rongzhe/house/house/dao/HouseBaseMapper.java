package com.rongzhe.house.house.dao;

import com.rongzhe.house.easypoi.dto.ContractInfo;
import com.rongzhe.house.house.criteria.HouseCriteria;
import com.rongzhe.house.house.entity.HouseBase;
import com.rongzhe.house.house.entity.HouseDetail;
import com.rongzhe.house.house.entity.HouseOverview;
import com.rongzhe.house.house.entity.HouseTag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface HouseBaseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseBase record);

    int insertSelective(HouseBase record);

    HouseBase selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseBase record);

    int updateByPrimaryKey(HouseBase record);

    List<HouseOverview> selectHouseByCriteria(HouseCriteria houseCriteria);


    List<HouseOverview> selectExcellentHouse(String cityId);


    HouseDetail selectDetailById(Integer houseId);

    HouseDetail selectDetailByIdc(Integer houseId);



    int countHouseByCriteria(HouseCriteria houseCriteria);

    List<HouseOverview> selectCollectionHouseByUid(@Param("uid") Integer uid, @Param("limit") Integer limit, @Param("offset") Integer offset);

    int countCollectionHouseByUid(@Param("uid") Integer uid);

    List<HouseOverview> selectHouseByIds(@Param("ids") List<Long> ids);


    List<HouseOverview> getExcellentHouse(HouseCriteria houseCriteria);

    HouseBase selectById(@Param("id") Integer id,@Param("uid")Integer uid);

    /**
     * @param houseId
     * @return
     */
    List<HouseTag> selectHouseTagById(@Param("houseId") Integer houseId);

    /**
     * 查询荣者 房源id
     * @param houseId 房源id
     * @return 结果
     */
    String selectRongzheAddressById(Integer houseId);

    /**
     *
     * @param houseId
     * @return
     */
    String selectAdress(Integer houseId);
}