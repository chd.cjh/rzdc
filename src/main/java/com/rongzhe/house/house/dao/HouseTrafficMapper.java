package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseTraffic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface HouseTrafficMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseTraffic record);

    int insertSelective(HouseTraffic record);

    HouseTraffic selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseTraffic record);

    int updateByPrimaryKey(HouseTraffic record);

    List<HouseTraffic> selectListByHouseId(Integer houseId);
}