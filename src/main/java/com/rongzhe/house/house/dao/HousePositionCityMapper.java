package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HousePositionCity;
import com.rongzhe.house.position.entity.HousePositionProvince;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HousePositionCityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HousePositionCity record);

    int insertSelective(HousePositionCity record);

    HousePositionCity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HousePositionCity record);

    int updateByPrimaryKey(HousePositionCity record);

    List<HousePositionCity> selectList();
}