package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.LifeService;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface LifeServiceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LifeService record);

    int insertSelective(LifeService record);

    LifeService selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LifeService record);

    int updateByPrimaryKey(LifeService record);

    List<LifeService> selectByType(@Param("type") Integer type);
}