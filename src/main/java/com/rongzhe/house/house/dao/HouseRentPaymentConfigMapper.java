package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseRentPaymentConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface HouseRentPaymentConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseRentPaymentConfig record);

    int insertSelective(HouseRentPaymentConfig record);

    HouseRentPaymentConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseRentPaymentConfig record);

    int updateByPrimaryKey(HouseRentPaymentConfig record);

    List<HouseRentPaymentConfig> selectHouseRentPayConfigListByHouseId(Integer houseId);
}