package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseRoom;

public interface HouseRoomMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseRoom record);

    int insertSelective(HouseRoom record);

    HouseRoom selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseRoom record);

    int updateByPrimaryKey(HouseRoom record);
}