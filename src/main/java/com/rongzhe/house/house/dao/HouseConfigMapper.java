package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface HouseConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseConfig record);

    int insertSelective(HouseConfig record);

    HouseConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseConfig record);

    int updateByPrimaryKey(HouseConfig record);

    HouseConfig selectByHouseId(@Param("houseId") Integer houseId);
}