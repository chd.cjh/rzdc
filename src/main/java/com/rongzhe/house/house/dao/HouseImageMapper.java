package com.rongzhe.house.house.dao;

import com.rongzhe.house.house.entity.HouseImage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HouseImageMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HouseImage record);

    int insertSelective(HouseImage record);

    HouseImage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HouseImage record);

    int updateByPrimaryKey(HouseImage record);

    List<HouseImage> selectListByHouseId(Integer houseId);
}