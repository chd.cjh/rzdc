package com.rongzhe.house.house.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class HouseRoomLiveInfo {
    private Integer id;

    private Integer houseId;

    private Integer liveUid;


    private Date startTime;

    private Date endTime;

    private Integer roomNum;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    /**
     * 职业
     */
    private String tenementProfession;

    /**
     * 性别
     */
    private String tenementSex;


    /**
     * 生日
     */
    private Date birthday;

    /**
     * 职业
     */
    private Integer age;

    /**
     * 面积
     */
    private BigDecimal area;

    /**
     * 居住人数
     */
    private Integer liveNum;


    private BigDecimal rent;

    private String rentStatus;
}