package com.rongzhe.house.house.entity;

import lombok.Data;

import java.util.Date;

@Data
public class HouseDelegationApply {
    private Integer id;

    private String realName;

    private String mobile;

    private String cityId;

    private String flag;

    private String neighborhoodName;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private String comment;

}