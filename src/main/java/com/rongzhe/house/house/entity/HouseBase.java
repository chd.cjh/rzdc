package com.rongzhe.house.house.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class HouseBase {
    private Integer id;

    private String address;

    private String code;

    private String name;

    private Boolean verifyStatus;

    private Date expireTime;

    private Date publishTime;

    private Byte deleted;

    private Byte isEntireRent;

    private Byte isOwnerRent;

    private Integer isApartment;

    private BigDecimal rent;

    private String roomNum;

    private BigDecimal area;

    private String regionFinal;

    private String region;

    private String cover;

    private String periphery;

    private Byte status;

    private String type;

    private Integer floor;

    private String orientation;

    private String latitude;

    private String longitude;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

    private Byte rentStatus;

    private Integer buildingNum;

    private Integer unitNum;

    private Integer houseNum;

    private String rentPayment;

}