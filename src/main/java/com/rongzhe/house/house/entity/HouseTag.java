package com.rongzhe.house.house.entity;

import lombok.Data;

/**
 * <p>Description:   </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author leaf
 * @version 1.0
 * @date 2018/6/25
 */
@Data
public class HouseTag {

    private Integer id;

    private String name;
}
