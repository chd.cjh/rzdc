package com.rongzhe.house.house.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by hggxg on 2017/11/18.
 */
@Data
public class HouseOverview {

    private String region;

    private String regionName;

    private String regionFinal;

    private String regionFinalName;

    private String cover;

    private BigDecimal area;

    private Boolean entireRent;

    private Boolean ownerRent;

    private Boolean apartment;

    private String name;

    private String type;

    private BigDecimal rent;

    private Integer id;

    private Date enterTime;

    private Date publishTime;

    private Integer rentStatus;

    private Integer floor;

    private Integer totalFloor;

    private List<HouseTraffic> houseTrafficList;

    private List<HouseTag> houseTags;

    private String cityId;

    private String latitude;

    private String longitude;

    private String roomNum;

    private Date insertTime;

    private String countyId;

    private String periphery;

    private String orientation;

    private Integer managerUid;

}
