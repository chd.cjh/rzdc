package com.rongzhe.house.house.entity;

import lombok.Data;

import java.util.Date;

@Data
public class HouseConfig {
    private Integer id;

    private Integer houseId;

    private Byte bed;

    private Byte wardrobe;

    private Byte desk;

    private Byte wifi;

    private Byte washwheel;

    private Byte heater;

    private Byte airCondition;

    private Byte microwaveOven;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

}