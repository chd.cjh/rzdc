package com.rongzhe.house.house.entity;

import com.rongzhe.house.user.entity.UserBase;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class HouseDetail extends HouseOverview {


    private UserBase managerInfo;

    private Boolean canCollection;

    private List<HouseImage> houseImageList;

    private List<HouseTraffic> houseTrafficList;

    private List<HouseTag> houseTags;

    private HouseConfig houseConfig;

    private List<HouseOverview> recommendHouseList;

    private List<HouseRoomLiveInfo> houseRoomLiveInfoList;

    private List<String> rentPaymentList;

    private Date contractStart;

    private Date contractEnd;

}
