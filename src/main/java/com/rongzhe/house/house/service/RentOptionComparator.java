package com.rongzhe.house.house.service;

import java.util.Comparator;

public class RentOptionComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {

        int a = Integer.parseInt(o1);
        int b = Integer.parseInt(o2);
        if (a == b) {
            return 0;
        }
        if (a > b) {
            return -1;
        }

        return 1;
    }
}
