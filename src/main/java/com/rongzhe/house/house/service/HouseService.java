package com.rongzhe.house.house.service;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.house.criteria.HouseCriteria;
import com.rongzhe.house.house.entity.*;
import com.rongzhe.house.house.request.DelegationApplyRequest;

import java.util.List;
import java.util.Map;

/**
 * Created by hggxg on 2017/11/16.
 */
public interface HouseService {


    /**
     * 获取优质房源
     *
     * @return
     */
    List<HouseOverview> getExcellentHouse(String provinceId);

    /**
     * 获取
     *
     * @param provinceId
     * @return
     */
    Map<String, List<HouseOverview>> getExcellentHouseOther(String provinceId);

    /**
     * 条件筛选
     *
     * @return
     */
    List<HouseOverview> queryHouseByCriteria(HouseCriteria houseCriteria);


    /**
     * 详情
     *
     * @param houseId
     * @param userId
     * @return
     */
    HouseDetail detail(Integer houseId, Integer userId);


    /**
     * 获取房源支付信息
     *
     * @param houseId
     * @return
     */
    List<HouseRentPaymentConfig> getHouseRentPayConfigListByHouseId(Integer houseId);


    /**
     * 收藏房源
     *
     * @param houseId
     * @param userId
     * @return
     */
    boolean collection(Integer houseId, Integer userId);


    /**
     * 获取收藏房源
     *
     * @param userId
     * @return
     */
    List<HouseOverview> collectionGet(Integer userId, Integer pageSize, Integer currentPage);


    /**
     * 房屋委托
     *
     * @param delegationApply
     * @return
     */
    boolean delegationApply(HouseDelegationApply delegationApply);


    /**
     * 获取生活服务内容
     *
     * @param type
     * @return
     */
    List<LifeService> getLifeService(Integer type);
    
}
