package com.rongzhe.house.house.es_repository;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Created by hggxg on 2018/1/2.
 */

@Data
public class ESHouse {


    private Integer id;

    /**
     * 名称
     */
    private String houseName;

    /**
     * 地址
     */
    private String address;

    /**
     * 区名称
     */
    private String regionName;

    /**
     * 最终区域名称
     */
    private String regionFinalName;

    /**
     * 地铁
     */
    private List<String> routeList;

    private String cityId;

    //维度
    private String lat;

    //经度
    private String lon;

    private boolean isEntireRent;

    private boolean isOwnerRent;

    private BigDecimal rent;

    private Integer type;

    private String orientation;

    private String enterTime;

    private Integer roomNum;


    private Set<String> railList;

    private Set<String> railStationList;

    private List<Double> location;

    private List<String> area;

    private String insertTime;

    private String countyId;

    private String code;

    private String rentStatus;
}
