package com.rongzhe.house.house.es_repository;


import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by hggxg on 2018/1/2.
 */
public interface ESHouseService {

    Iterable<ESHouse> save(Iterable<ESHouse> esHouses) throws IOException, ExecutionException, InterruptedException;


}
