package com.rongzhe.house.house.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.rest.BaseController;
import com.rongzhe.house.contract.entity.HouseReturnApply;
import com.rongzhe.house.contract.service.HouseReturnApplyService;
import com.rongzhe.house.house.criteria.HouseCriteria;
import com.rongzhe.house.house.entity.HouseDelegationApply;
import com.rongzhe.house.house.entity.HouseDetail;
import com.rongzhe.house.house.entity.HouseOverview;
import com.rongzhe.house.house.entity.HouseRentPaymentConfig;
import com.rongzhe.house.house.request.HouseRequest;
import com.rongzhe.house.house.service.HouseService;
import com.rongzhe.house.user.entity.UserElement;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by hggxg on 2017/11/18.
 */
@Slf4j
@RestController
@RequestMapping("house")
public class HouseController extends BaseController {

    @Autowired
    private HouseService houseService;

    @ApiOperation(value = "房源检索", notes = "房源", httpMethod = "POST")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    @ApiImplicitParam(name = "houseCriteria", value = "检索条件", required = true, dataType = "HouseCriteria")
    public ApiResult<List<HouseOverview>> houseQuery(@RequestBody HouseCriteria houseCriteria) {
        return ApiResult.buildOk(houseService.queryHouseByCriteria(houseCriteria));
    }


    @ApiOperation(value = "获取收藏房源", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/collection/list", method = RequestMethod.GET)
    @ApiImplicitParam(name = "houseCriteria", value = "检索条件", required = true, dataType = "HouseCriteria")
    public ApiResult<List<HouseOverview>> collectionGet(@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                        @RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage) {

        ApiResult<List<HouseOverview>> apiResult = new ApiResult();


        apiResult.setData(houseService.collectionGet(getCurrentUser().getUserId(), pageSize, currentPage));


        return apiResult;

    }


    @ApiOperation(value = "首页优质房源获取", notes = "首页", httpMethod = "POST")
    @RequestMapping(value = "/excellent/query", method = RequestMethod.POST)
    @ApiImplicitParam(name = "houseCriteria", value = "检索条件", required = true, dataType = "HouseCriteria")
    public ApiResult<List<HouseOverview>> houseExcellentQuery(@RequestBody HouseCriteria houseCriteria) {
        ApiResult apiResult = new ApiResult();

        apiResult.setData(houseService.getExcellentHouse(houseCriteria.getCityId()));


        return apiResult;
    }


    @ApiOperation(value = "首页优质房源 整租 直租 合租", notes = "首页", httpMethod = "POST")
    @RequestMapping(value = "/excellent/house/query", method = RequestMethod.POST)
    @ApiImplicitParam(name = "houseCriteria", value = "检索条件", required = true, dataType = "HouseCriteria")
    public ApiResult<Map<String, List<HouseOverview>>> houseExcellentHouseQuery(@RequestBody HouseCriteria houseCriteria) {
        ApiResult apiResult = new ApiResult();
        apiResult.setData(houseService.getExcellentHouseOther(houseCriteria.getCityId()));
        return apiResult;
    }

    @ApiOperation(value = "详情", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ApiImplicitParam(name = "houseId", value = "房子ID", required = true, dataType = "Integer")
    public ApiResult<HouseDetail> detail(@RequestParam(value = "houseId") Integer houseId) {

        ApiResult apiResult = new ApiResult();

        UserElement userElement = getCurrentUser();

        apiResult.setData(houseService.detail(houseId, userElement == null ? null : userElement.getUserId()));


        return apiResult;
    }

    @ApiOperation(value = "可选支付方式（月，季，年）", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/payConfig", method = RequestMethod.GET)
    @ApiImplicitParam(name = "houseId", value = "房子ID", required = true, dataType = "Integer")
    public ApiResult<List<HouseRentPaymentConfig>> payConfig(@RequestParam(value = "houseId") Integer houseId) {

        ApiResult apiResult = new ApiResult();

        apiResult.setData(houseService.getHouseRentPayConfigListByHouseId(houseId));

        return apiResult;
    }


    @ApiOperation(value = "收藏", notes = "房源", httpMethod = "POST")
    @RequestMapping(value = "/collection", method = RequestMethod.POST)
    @ApiImplicitParam(name = "houseRequest", value = "房子ID", required = true, dataType = "HouseRequest")
    public ApiResult collection(@RequestBody HouseRequest houseRequest) {

        ApiResult apiResult = new ApiResult();

        apiResult.setData(houseService.collection(houseRequest.getHouseId(), getCurrentUser().getUserId()));


        return apiResult;
    }


    @ApiOperation(value = "委托申请", notes = "房源", httpMethod = "POST")
    @RequestMapping(value = "/delegation/apply", method = RequestMethod.POST)
    @ApiImplicitParam(name = "delegationApply", value = "申请", required = true, dataType = "HouseDelegationApply")
    public ApiResult delegationApply(@RequestBody HouseDelegationApply delegationApply) {

        ApiResult apiResult = new ApiResult();

        apiResult.setData(houseService.delegationApply(delegationApply));


        return apiResult;
    }


    @ApiOperation(value = "生活服务", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/lifeService/list", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "房子ID", required = true, dataType = "Integer")
    public ApiResult lifeServiceList(@RequestParam(value = "type") Integer type) {

        ApiResult apiResult = new ApiResult();

        apiResult.setData(houseService.getLifeService(type));


        return apiResult;
    }

    @Autowired
    private HouseReturnApplyService houseReturnApplyService;

    @ApiOperation(value = "退房申请", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/return/apply", method = RequestMethod.POST)
    public ApiResult houseReturnApply(@RequestBody HouseReturnApply houseReturnApply) {
        //退租申请
        return houseReturnApplyService.insertRecord(houseReturnApply);
    }


    @ApiOperation(value = "退房申请详情", notes = "房源", httpMethod = "GET")
    @RequestMapping(value = "/return/applyGet", method = RequestMethod.GET)
    public ApiResult houseReturnGet(@RequestParam("contractId") Integer id) {

        //退租申请
//        houseReturnApply.setUid(getCurrentUser().getUserId());

        return houseReturnApplyService.selectByContractId(id);
    }


}
