package com.rongzhe.house.house.request;

import lombok.Data;

/**
 * Created by hggxg on 2017/12/11.
 */
@Data
public class HouseRequest {

    private Integer houseId;

}
