package com.rongzhe.house.house.request;

import lombok.Data;

/**
 * Created by hggxg on 2017/12/11.
 */
@Data
public class DelegationApplyRequest {

    /**
     * 姓名
     */
    private String realName;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 城市
     */
    private String cityId;

    /**
     * 小区名称
     */
    private String neighborhoodName;

}
