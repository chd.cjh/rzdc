package com.rongzhe.house.house.enums;

/**
 * Created by hggxg on 2018/1/21.
 */
public enum HouseBaseRentStatus {

    //
    // ;
    CAN_ENTER(1),

    CAN_NOT_ENTER(2),

    LOCK(3);


    private int status;

    HouseBaseRentStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
