package com.rongzhe.house.house.enums;

/**
 * Created by hggxg on 2017/12/11.
 */
public enum UserCollectionType {

    HOUSE(1);

    private int type;

    UserCollectionType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
