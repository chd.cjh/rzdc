package com.rongzhe.house.house.vo;

import com.alibaba.fastjson.JSON;
import com.rongzhe.house.order.entity.OrderSnapshotContent;
import lombok.Data;

import java.util.Date;

/**
 * <p>Description:  退租明细 </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author yangpeng
 * @version 1.0
 * @date 2018/3/16
 * @since 1.8
 */
@Data
public class HouseReturnApplyVO {

    /**
     * 退租id
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 合同id
     */
    private Integer contractId;


    /**
     * 退租申请的状态
     */
    private String status;

    /**
     * 退租申请的原因
     */
    private String reason;

    /**
     * 合同开始时间
     */
    private Date startTime;

    /**
     * 合同结束时间
     */
    private Date endTime;

    /**
     * 应退押金
     */
    private String returnDeposit;

    /**
     * 应退租金
     */
    private String returnRent;

    /**
     * 扣除费用
     */
    private String returnDeduction;

    /**
     * 申请的 备注1
     */
    private String remark;

    /**
     * 详细的 备注2
     */
    private String remark2;

    /**
     * 银行卡号
     */
    private String accountNum;

    /**
     * 开户行地址
     */
    private String accountLocation;

    /**
     * 结果
     */
    private String result;

    /**
     * 管家姓名
     */
    private String managerName;

    /**
     * 租客姓名
     */
    private String rentName;


    /**
     * 快照信息
     */
    private OrderSnapshotContent snapshot;

    private String snapshots;

    public void setSnapshot(String snapshot) {
        // 快照信息
        OrderSnapshotContent orderSnapshotContent = JSON.parseObject(snapshot, OrderSnapshotContent.class);
        this.snapshot = orderSnapshotContent;
    }
}
