package com.rongzhe.house.house.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by hggxg on 2017/11/16.
 */
@Data
public class HouseCriteria extends BaseParam {


    private String keyword;

    private String provinceId;

    private String cityId;

    private String countyId;

    /**
     * 跌帖路线
     */
    private String route;

    /**
     * 站名
     */
    private String station;

    /**
     * 租金价格区间值
     */
    private BigDecimal rentStart;


    /**
     * 租金价格区间值
     */
    private BigDecimal rentEnd;

    private Integer type;

    /**
     * 是否整租
     */
    private Boolean entireRent;

    /**
     * 是否直租
     */
    private Boolean ownerRent;

    /**
     * 是否是公寓
     */
    private boolean apartment;

    /**
     * 几居室 （来源：数据字典）
     */
    private String roomNum;


    /**
     * 朝向 （来源：数据字典）
     */
    private String orientation;


    /**
     * 入住时间 （大于等于）
     */
    private Date enterTime;

    private List<Integer> tags;

    /**
     * 周围
     */
    private boolean findRound;

    /**
     * 当前纬度
     */
    private Double lat;

    /**
     * 当前经度
     */
    private Double lon;

    /**
     * 居室
     */
    private List<String> roomNumList;


}
