package com.rongzhe.house.easypoi.util;


import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.utils.FileUtil;
import com.rongzhe.house.easypoi.constant.FileConstants;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * <p>Description: 下载网络图片  </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author leaf
 * @version 1.0
 * @date 2018/8/1
 */
@Slf4j
public class DownloadImage {

    /**
     * 下载网络图片
     *
     * @param urlString 网络图片地址
     * @return
     */
    public static String download(String urlString) {
        String savePath = FileConstants.PATH_IMAGE;
        String fileName = FileUtil.getNamePart(urlString);
        try {
            download(urlString, fileName, savePath);
            return savePath + File.separator + fileName;
        } catch (Exception e) {
            log.error("图片下载失败", e);
            throw new RongZheBusinessException("图片下载失败:" + urlString);
        }
    }

    /**
     * @param urlString
     * @param filename
     * @param savePath
     * @throws Exception
     */
    public static void download(String urlString, String filename, String savePath) throws Exception {
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        //设置请求超时为5s
        con.setConnectTimeout(5 * 1000);
        // 输入流
        InputStream is = con.getInputStream();

        // 1K的数据缓冲
        byte[] bs = new byte[1024];
        // 读取到的数据长度
        int len;
        // 输出的文件流
        File sf = new File(savePath);
        if (!sf.exists()) {
            sf.mkdirs();
        }
        OutputStream os = new FileOutputStream(sf.getPath() + File.separator + filename);
        // 开始读取
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        // 完毕，关闭所有链接
        os.close();
        is.close();
    }
}
