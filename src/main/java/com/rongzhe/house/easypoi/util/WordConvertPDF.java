package com.rongzhe.house.easypoi.util;

import com.aspose.words.Document;
import com.aspose.words.SaveFormat;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * <p>Description: word文件转换成为pdf文件  </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
public class WordConvertPDF {

    /**
     * 把word文件转换成pdf文件
     *
     * @param fileInput 输入的文件流
     * @throws Exception
     */
    public static void converFile(InputStream fileInput, ByteArrayOutputStream fileOS) throws Exception {
        Document doc = new Document(fileInput);
        doc.save(fileOS, SaveFormat.PDF);
    }

}
