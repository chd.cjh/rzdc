package com.rongzhe.house.easypoi.constant;

import org.springframework.util.ResourceUtils;

import java.io.File;

/**
 * <p>Description: 获取文件路径  </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
public class FileConstants {

    /**
     * 网络图片存放路径
     */
    public static String PATH_IMAGE;
    /**
     * 合同临时存储路径
     */
    public static String PATH_TEMP_CONTRACT;
    /**
     * 合同存储路径
     */
    public static String PATH_CONTRACT_PDF;

    /**
     * 合同模板路径
     */
    public static String PATH_TEMPLATE_CONTRACT;

    static {
        File path = null;
        try {
            path = new File(ResourceUtils.getURL("classpath:").getPath());
//            PATH_IMAGE = path.getAbsolutePath() + File.separator + "temp" + File.separator + "upload";
//            PATH_TEMP_CONTRACT = path.getAbsolutePath() + File.separator + "temp" + File.separator + "tempContract";
//            PATH_CONTRACT_PDF = path.getAbsolutePath() + File.separator + "temp" + File.separator + "contract";
            PATH_IMAGE = "temp" + File.separator + "upload";
            PATH_TEMP_CONTRACT = "temp" + File.separator + "tempContract";
            PATH_CONTRACT_PDF = "temp" + File.separator + "contract";
            PATH_TEMPLATE_CONTRACT = "doc" + File.separator + "重庆荣者房屋租赁服务合同.docx";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
