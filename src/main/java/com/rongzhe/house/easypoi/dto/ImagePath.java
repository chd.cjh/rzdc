package com.rongzhe.house.easypoi.dto;

import lombok.Data;

/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
@Data
public class ImagePath {
    /**
     * 身份证正面
     */
    private String certificateImage1;
    /**
     * 身份证反面
     */
    private String certificateImage2;
}
