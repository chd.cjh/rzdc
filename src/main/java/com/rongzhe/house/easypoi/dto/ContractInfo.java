package com.rongzhe.house.easypoi.dto;

import lombok.Data;

/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
@Data
public class ContractInfo {
    private String rongzheAddress;
    private String area;
}
