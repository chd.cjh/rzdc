package com.rongzhe.house.easypoi.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
@Data
public class RentInfoDTO {

    /**
     * 租客姓名
     */
    private String rentName;

    /**
     * 性别
     */
    private String rentSex;

    /**
     * 租客电话
     */
    private String rentMobile;

    /**
     * 身份证号码
     */
    private String certificateNo;
    /**
     * 身份证正面
     */
    private String certificateImage1;
    /**
     * 身份证反面
     */
    private String certificateImage2;

    /**
     * 开始时间
     */
    private Date rentStart;
    /**
     * 结束时间
     */
    private Date rentEnd;

    /**
     * 租期
     */
    private String rentTerm;

    /**
     * 付款方式
     */
    private String payMethod;

    private String payCount;
    /**
     * 押金
     */
    private BigDecimal deposit;

    /**
     * 每月租金
     */
    private BigDecimal monthRent;

    /**
     * 服务费
     */
    private BigDecimal monthFee;


    private String area;

    private String rongzheAddress;

    /**
     * 房源id
     */
    private Integer houseId;

    /**
     * 房源地址
     */
    private String houseAddress;

    /**
     * 合同备注
     */
    private String contractRemark;

    /**
     * 公共家具清单
     */
    private String publicList;

    /**
     * 房间家具清单
     */
    private String privateList;

    /**
     * 其他家具清单
     */
    private String otherList;
}
