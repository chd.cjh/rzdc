package com.rongzhe.house.easypoi.handler;

import com.rongzhe.house.common.utils.FileUtils;
import com.rongzhe.house.easypoi.dto.ImagePath;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/2
 */
@Slf4j
@Component
public class DeleteFileHandler {

    @Async
    public void deleteFileInfo(ImagePath imagePath) {
        try {
            if (imagePath != null) {
                if (StringUtils.isNotBlank(imagePath.getCertificateImage1())) {
                    FileUtils.deleteFile(imagePath.getCertificateImage1());
                }
                if (StringUtils.isNotBlank(imagePath.getCertificateImage2())) {
                    FileUtils.deleteFile(imagePath.getCertificateImage2());
                }
            }
        } catch (Exception e) {
            log.error("删除合同临时信息失败", e);
        }
    }
}
