package com.rongzhe.house.easypoi.config;

import com.aspose.words.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.InputStream;

/**
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * @author admin
 * @version 1.0
 * @date 2018/8/1
 */
@Configuration
public class AposeConfig {

    private static InputStream license;

    @Bean
    public License getAposeLic() throws Exception {
        License aposeLic = new License();
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        File path = new File(ResourceUtils.getURL("classpath:apose/license.xml").getPath());
//        path = loader.getResource("apose/license.xml").getPath();
        // 凭证文件
        license = this.getClass().getResourceAsStream("/apose/license.xml");
        aposeLic.setLicense(license);
        return aposeLic;
    }
}
