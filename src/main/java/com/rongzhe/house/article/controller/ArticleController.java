package com.rongzhe.house.article.controller;

import com.rongzhe.house.article.criteria.ArticleCriteria;
import com.rongzhe.house.article.service.ArticleService;
import com.rongzhe.house.common.resp.ApiResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Chengjia
 * @date 2018/3/3 11:33
 */
@RequestMapping("article")
@RestController
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @ApiOperation(value = "文章列表", notes = "article", httpMethod = "POST")
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public ApiResult list(@RequestBody ArticleCriteria articleCriteria){
        return articleService.selectWithPage(articleCriteria);
    }
}
