package com.rongzhe.house.article.dao;

import com.rongzhe.house.article.bo.ArticleBo;
import com.rongzhe.house.article.criteria.ArticleCriteria;
import com.rongzhe.house.article.entity.Article;
import com.rongzhe.house.tag.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文章
 */
@Mapper
@Repository
public interface ArticleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Integer id);
    ArticleBo selectById(Integer id);

    List<Integer> selectArticleTagIds(Integer articleId);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKeyWithBLOBs(Article record);

    int updateByPrimaryKey(Article record);

    long countByCriteria(ArticleCriteria articleCriteria);

    List<ArticleBo> selectByCriteria(ArticleCriteria articleCriteria);

    List<Tag> selectArticleTagList(Integer articleId);

}