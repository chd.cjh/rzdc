package com.rongzhe.house.article.bo;

import com.rongzhe.house.article.entity.Article;

import java.util.List;

/**
 * 文章
 * @author Chengjia
 * @date 2018/3/3 14:55
 */
public class ArticleBo extends Article {
    private List<Integer> newTagId;//新标签
    private String author;

    public List<Integer> getNewTagId() {
        return newTagId;
    }

    public void setNewTagId(List<Integer> newTagId) {
        this.newTagId = newTagId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
