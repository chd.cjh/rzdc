package com.rongzhe.house.article.service;

import com.alibaba.fastjson.JSONObject;
import com.rongzhe.house.article.bo.ArticleBo;
import com.rongzhe.house.article.criteria.ArticleCriteria;
import com.rongzhe.house.article.dao.ArticleMapper;
import com.rongzhe.house.article.entity.Article;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.utils.PageUtils;
import com.rongzhe.house.tag.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chengjia
 * @date 2018/3/3 11:36
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public ApiResult selectWithPage(ArticleCriteria criteria) {
        ApiResult result=new ApiResult();
        long count=articleMapper.countByCriteria(criteria);
        if(count==0){
            result.setCode(200);
            result.setMessage("未查到相关的文章");
            return result;
        }
        PageUtils.initPageParam(criteria);
        List<ArticleBo> articles = articleMapper.selectByCriteria(criteria);
        for (Article article:articles){
            List<Tag> tags = articleMapper.selectArticleTagList(article.getId());
            article.setArticleTags(tags);
        }
        JSONObject param=new JSONObject();
        result.setCode(200);
        result.setMessage("查询相关文章成功");
        param.put("total",count);
        param.put("data",articles);
        result.setData(param);
        return result;
    }

}
