package com.rongzhe.house.article.service;

import com.rongzhe.house.article.criteria.ArticleCriteria;
import com.rongzhe.house.common.resp.ApiResult;

/**
 * @author Chengjia
 * @date 2018/3/3 11:36
 */
public interface ArticleService {

    /**
     * 分页查询
     * @param criteria
     * @return
     */
    ApiResult selectWithPage(ArticleCriteria criteria);
}
