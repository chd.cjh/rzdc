package com.rongzhe.house.article.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import lombok.Data;

/**
 * @author Chengjia
 * @date 2018/3/3 12:02
 */
@Data
public class ArticleCriteria extends BaseParam{
    private String title;
    private String publishStatus;
    private int typeId;
}
