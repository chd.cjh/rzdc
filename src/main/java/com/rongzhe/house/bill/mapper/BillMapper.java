package com.rongzhe.house.bill.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.rongzhe.house.bill.criteria.BillCriteria;
import com.rongzhe.house.bill.entity.Bill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>Description: 账单表  Mapper 接口 </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author HeGang123
 * @version 1.0
 * @since 2018-04-02
 */
@Repository
@Mapper
public interface BillMapper extends BaseMapper<Bill> {

    List<Bill> selectByContractId(@Param("contractId") Integer contractId);

    /**
     * 查询租客账单
     * @param billCriteria
     * @return
     */
    List<Bill> queryBillList(BillCriteria billCriteria);

    /**
     * 生活账单
     * @param billCriteria
     * @return
     */
    List<Bill> queryLifeBillList(BillCriteria billCriteria);
}