package com.rongzhe.house.bill.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.rongzhe.house.bill.entity.BillImage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 *
 * <p>Description:   Mapper 接口 </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author HeGang123
 * @version 1.0
 * @since 2018-04-10
 *
 */
@Repository
@Mapper
public interface BillImageMapper extends BaseMapper<BillImage> {

}