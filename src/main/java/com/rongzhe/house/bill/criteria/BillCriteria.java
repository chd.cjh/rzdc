package com.rongzhe.house.bill.criteria;

import com.rongzhe.house.common.rest.BaseParam;
import lombok.Data;

import java.util.Date;

/**
 * 账单查询参数
 * 2018-4-27 10:57:35
 */
@Data
public class BillCriteria extends BaseParam {
    private Integer id;
    private String regionName;
    private String finalRegionName;
    private String houseName;
    private Integer houseId;
    private double price;
    private String rentPayment;
    private Integer uId;
    private String name;
    private double payable;
    private Integer status;
    private  Date payableDate;
    private  Date startTime;
    private  Date endTime;
    private String cover;
}

