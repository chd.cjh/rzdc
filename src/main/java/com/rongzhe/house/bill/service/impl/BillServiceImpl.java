package com.rongzhe.house.bill.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rongzhe.house.bill.bo.BillBo;
import com.rongzhe.house.bill.criteria.BillCriteria;
import com.rongzhe.house.bill.entity.Bill;
import com.rongzhe.house.bill.mapper.BillMapper;
import com.rongzhe.house.bill.service.BillService;
import com.rongzhe.house.common.utils.PageUtils;
import com.rongzhe.house.house.dao.HouseBaseMapper;
import com.rongzhe.house.house.entity.HouseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 账单表 服务实现类
 * </p>
 *
 * @author ChengJia123
 * @since 2018-04-24
 */
@Service
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill> implements BillService {
    @Autowired
    private BillMapper billMapper;

    @Autowired
    private HouseBaseMapper houseBaseMapper;
    @Override
    public BillBo search(BillCriteria billCriteria,Integer currentUserId) {
        billCriteria.setUId(currentUserId);
        PageUtils.initPageParam(billCriteria);
        List<Bill> bills = billMapper.queryBillList(billCriteria);
        HouseBase houseBase = houseBaseMapper.selectById(billCriteria.getHouseId(), currentUserId);

        BillBo billBo=new BillBo();
        billBo.setBills(bills);
        billBo.setHouseBase(houseBase);
        return billBo;
    }

    @Override
    public BillBo queryLifeBillList(BillCriteria billCriteria, Integer currentUserId) {
        billCriteria.setUId(currentUserId);
        PageUtils.initPageParam(billCriteria);
        List<Bill> bills = billMapper.queryLifeBillList(billCriteria);

        HouseBase houseBase = houseBaseMapper.selectById(billCriteria.getHouseId(), currentUserId);
        BillBo billBo=new BillBo();
        billBo.setBills(bills);
        billBo.setHouseBase(houseBase);
        return billBo;
    }
}
