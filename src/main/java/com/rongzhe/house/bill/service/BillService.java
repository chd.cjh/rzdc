package com.rongzhe.house.bill.service;

import com.baomidou.mybatisplus.service.IService;
import com.rongzhe.house.bill.bo.BillBo;
import com.rongzhe.house.bill.criteria.BillCriteria;
import com.rongzhe.house.bill.entity.Bill;

/**
 * <p>
 * 账单表 服务类
 * </p>
 *
 * @author ChengJia123
 * @since 2018-04-24
 */
public interface BillService extends IService<Bill> {
    /**
     * 租客账单
     * @param billCriteria
     * @return
     */
    BillBo search(BillCriteria billCriteria, Integer currentUserId);

    /**
     * 生活账单
     * @param billCriteria
     * @param currentUserId
     * @return
     */
    BillBo queryLifeBillList(BillCriteria billCriteria,Integer currentUserId);
}
