package com.rongzhe.house.bill.bo;

import com.rongzhe.house.bill.entity.Bill;
import com.rongzhe.house.house.entity.HouseBase;
import lombok.Data;

import java.util.List;

/**
 * @author Chengjia
 * @date 2018/5/25 10:59
 */
@Data
public class BillBo {
    private HouseBase houseBase;
    private List<Bill> bills;
}
