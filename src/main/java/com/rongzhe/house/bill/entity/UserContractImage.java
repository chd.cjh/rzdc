package com.rongzhe.house.bill.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * <p>Description: 房东合同 图片地址   </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author HeGang123
 * @version 1.0
 * @since 2018-04-10
 *
 */
@Data
@Accessors(chain = true)
@TableName("user_contract_image")
public class UserContractImage extends Model<UserContractImage> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 合同Id
     */
	@TableField("contract_id")
	private Integer contractId;
    /**
     * 合同的图片地址
     */
	@TableField("contract_picture")
	private String contractPicture;
    /**
     * 插入时间
     */
	@TableField("insert_time")
	private Date insertTime;
    /**
     * 插入人uid
     */
	@TableField("insert_uid")
	private Integer insertUid;
    /**
     * 修改时间
     */
	@TableField("update_time")
	private Date updateTime;
    /**
     * 修改人uid
     */
	@TableField("update_uid")
	private Integer updateUid;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
