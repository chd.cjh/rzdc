package com.rongzhe.house.bill.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>Description: 账单表   </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author HeGang123
 * @version 1.0
 * @since 2018-04-02
 */
@Data
@Accessors(chain = true)
@TableName("bill")
public class Bill extends Model<Bill> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    /**
     * 账单所属人
     */
    private Integer uid;

    /**
     * 名称
     */
    private String name;

    /**
     *
     */
    private String remark;

    /**
     * 应付金额
     */
    private BigDecimal payable;
    /**
     * 应付款日期
     */
    @TableField("payable_date")
    private Date payableDate;
    private BigDecimal received;
    @TableField("received_date")
    private Date receivedDate;
    /**
     * 1 代收款 2 已收款 3 作废
     */
    private String status;
    /**
     * 0 未删除 1 删除
     */
    private Integer deleted;
    private String type;
    @TableField("parent_id")
    private Integer parentId;
    /**
     * 业务id
     */
    @TableField("biz_id")
    private Integer bizId;
    /**
     * 1合同
     */
    @TableField("biz_type")
    private String bizType;
    /**
     * 是否核销 0 否 1 是
     */
    @TableField("is_confirm")
    private Integer isConfirm;
    @TableField("confirm_uid")
    private Integer confirmUid;
    /**
     * 插入时间
     */
    @TableField("insert_time")
    private Date insertTime;
    /**
     * 插入人uid
     */
    @TableField("insert_uid")
    private Integer insertUid;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 修改人uid
     */
    @TableField("update_uid")
    private Integer updateUid;

    @TableField("start_time")
    private Date startTime;

    @TableField("end_time")
    private Date endTime;


    @TableField(exist = false)
    private List<Bill> children;


    /**
     * 租客姓名
     */
    @TableField(exist = false)
    private String realName;

    /**
     * 租客手机号
     */
    @TableField(exist = false)
    private String mobile;

    /**
     * 租客身份证号
     */
    @TableField(exist = false)
    private String idCardNumber;

    /**
     * 合同图片
     */
    @TableField(exist = false)
    private List<String> images;

    /**
     * 商品
     */
    @TableField(exist = false)
    private Integer itemId;

    /**
     * 开始时间
     */
    @TableField(exist = false)
    private Date startDate;

    /**
     * 结束时间
     */
    @TableField(exist = false)
    private Date endDate;

    /**
     * 月份数量
     */
    @TableField(exist = false)
    private Integer monthCount;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
