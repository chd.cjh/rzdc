package com.rongzhe.house.bill.controller;


import com.rongzhe.house.bill.criteria.BillCriteria;
import com.rongzhe.house.bill.service.BillService;
import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.common.rest.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 账单表 前端控制器
 * </p>
 *
 * @author ChengJia123
 * @since 2018-04-24
 */
@RestController
@RequestMapping("/bill")
public class BillController extends BaseController {

    @Autowired
    private BillService billService;

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ApiResult<String> query(@RequestBody BillCriteria billCriteria) {
        return ApiResult.buildOk(billService.search(billCriteria, getCurrentUser().getUserId()));
    }

    @RequestMapping(value = "/queryLife", method = RequestMethod.POST)
    public ApiResult<String> queryLife(@RequestBody BillCriteria billCriteria) {
        return ApiResult.buildOk(billService.queryLifeBillList(billCriteria, getCurrentUser().getUserId()));
    }
}

