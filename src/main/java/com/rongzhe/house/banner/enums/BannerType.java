package com.rongzhe.house.banner.enums;

/**
 * Created by hggxg on 2017/11/16.
 */
public enum BannerType {


    INDEX("1");


    BannerType(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    public static BannerType getBannerType(String type) {

        for (BannerType bannerType : BannerType.values()) {
            if (bannerType.getType().equals(type)) {
                return bannerType;
            }
        }
        return BannerType.INDEX;
    }

}
