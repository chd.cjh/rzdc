package com.rongzhe.house.banner.controller;

import com.rongzhe.house.banner.entity.Banner;
import com.rongzhe.house.banner.enums.BannerType;
import com.rongzhe.house.banner.service.BannerService;
import com.rongzhe.house.common.constants.Constants;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.resp.ApiResult;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hggxg on 2017/11/18.
 */
@Slf4j
@RestController
@RequestMapping("banner")
public class BannerController {


    @Autowired
    private BannerService bannerService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "获取banner", notes = "首页", httpMethod = "GET")
    @ApiImplicitParam(name = "cityId", value = "检索条件", required = true, dataType = "String")
    public ApiResult<List<Banner>> list(@RequestParam(value = "cityId") String cityId,
                                        @RequestParam(value = "type", defaultValue = "1") String type) {
        ApiResult apiResult = new ApiResult();

        try {
            apiResult.setData(bannerService.getBannerListByType(BannerType.getBannerType(type), cityId));
        } catch (RongZheBusinessException e) {
            log.error("获取Banner异常", e);
            apiResult.setCode(Constants.RESP_STATUS_INTERNAL_ERROR);
            apiResult.setMessage("服务器异常");
        }

        return apiResult;
    }


}
