package com.rongzhe.house.banner.service;

import com.rongzhe.house.banner.entity.Banner;
import com.rongzhe.house.banner.enums.BannerType;
import com.rongzhe.house.common.exception.RongZheBusinessException;

import java.util.List;

/**
 * Created by hggxg on 2017/11/16.
 */
public interface BannerService {


    /**
     * 获取banner列表
     *
     * @return
     */
    List<Banner> getBannerListByType(BannerType bannerType, String provinceId) throws RongZheBusinessException;


}
