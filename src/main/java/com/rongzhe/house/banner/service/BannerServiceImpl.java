package com.rongzhe.house.banner.service;

import com.rongzhe.house.banner.dao.BannerMapper;
import com.rongzhe.house.banner.entity.Banner;
import com.rongzhe.house.banner.enums.BannerType;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hggxg on 2017/11/16.
 */
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<Banner> getBannerListByType(BannerType bannerType, String provinceId) throws RongZheBusinessException {

        return bannerMapper.selectByTypeAndProvinceId(bannerType.getType(), provinceId);

    }
}
