package com.rongzhe.house.banner.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Banner {
    private Integer id;

    private String type;

    private String cityId;

    private Byte isShow;

    private String remark;

    private String url;

    private Integer bizType;

    private Integer bizId;

    private String bizContent;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

}