package com.rongzhe.house.dictionary.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Dictionary {
    private Integer id;

    private String ext;

    private String code;

    private String parentCode;

    private String name;

    private Date insertTime;

    private Integer insertUid;

    private Date updateTime;

    private Integer updateUid;

}