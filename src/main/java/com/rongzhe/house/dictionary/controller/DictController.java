package com.rongzhe.house.dictionary.controller;

import com.rongzhe.house.common.resp.ApiResult;
import com.rongzhe.house.dictionary.entity.Dictionary;
import com.rongzhe.house.dictionary.service.DictService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hggxg on 2017/12/5.
 */
@RestController
@RequestMapping("dict")
public class DictController {

    @Autowired
    DictService dictService;

    @RequestMapping(value = "/getDictByParentCode", method = RequestMethod.GET)
    @ApiOperation(value = "根据parentCode获取数据字典", notes = "数据字典", httpMethod = "GET")
    @ApiImplicitParam(name = "parentCode", value = "parentCode", required = true, dataType = "String")
    public ApiResult<List<Dictionary>> getDictByParentCode(@RequestParam String parentCode) {


        ApiResult<List<Dictionary>> listApiResult = new ApiResult<>();
        listApiResult.setData(dictService.getDictByParentCode(parentCode));

        return listApiResult;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation(value = "获取所有", notes = "数据字典", httpMethod = "GET")
    public ApiResult<List<Dictionary>> getAll() {


        ApiResult<List<Dictionary>> listApiResult = new ApiResult<>();
        listApiResult.setData(dictService.getAll());

        return listApiResult;
    }
}
