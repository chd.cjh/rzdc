package com.rongzhe.house.dictionary.service;

import com.rongzhe.house.dictionary.dao.DictionaryMapper;
import com.rongzhe.house.dictionary.entity.Dictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hggxg on 2017/12/5.
 */
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictionaryMapper dictionaryMapper;

    @Override
    public List<Dictionary> getDictByParentCode(String parentCode) {


        return dictionaryMapper.selectByParentCode(parentCode);
    }

    @Override
    public List<Dictionary> getAll() {
        return dictionaryMapper.selectAll();
    }

    @Override
    public Map<String, String> getMapByParentCode(String parentCode) {

        List<Dictionary> dictionaries = dictionaryMapper.selectByParentCode(parentCode);

        Map<String, String> map = new HashMap<>();

        if (!CollectionUtils.isEmpty(dictionaries)) {
            for (Dictionary dictionary : dictionaries) {
                map.put(dictionary.getCode(), dictionary.getName());
            }
        }


        return map;
    }
}
