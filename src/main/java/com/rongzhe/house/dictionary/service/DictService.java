package com.rongzhe.house.dictionary.service;

import com.rongzhe.house.dictionary.entity.Dictionary;

import java.util.List;
import java.util.Map;

/**
 * Created by hggxg on 2017/12/5.
 */
public interface DictService {


    List<Dictionary> getDictByParentCode(String parentCode);

    List<Dictionary> getAll();

    Map<String, String> getMapByParentCode(String parentCode);

}
