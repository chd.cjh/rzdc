package com.rongzhe.house.dictionary.enums;

/**
 * Created by hggxg on 2017/12/5.
 */
public enum DictEnums {

    /**
     * 朝向
     */
    HOUSE_ORIENTATION("House_Orientation"),
    /**
     * 几居室
     */
    HOUSE_ROOM_NUM("House_Room_Num"),
    /**
     * 租金支付方式
     */
    House_Rent_Pay_Type("House_Rent_Pay_Type");


    DictEnums(String code) {
        this.code = code;
    }

    private String code;

    public String getCode() {
        return code;
    }


    /**
     * 判断是不是有效的字典
     *
     * @param code
     * @return
     */
    public static boolean validEnum(String code) {
        DictEnums[] dictEnums = DictEnums.values();

        for (DictEnums enums : dictEnums) {
            if (enums.getCode().equals(code)) {
                return true;
            }
        }
        return false;

    }

}
