package com.rongzhe.house;

import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Description:   </p>
 * <p>email: ypasdf@163.com</p>
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * @author yangpeng
 * @version 1.0
 * @date 2018/1/6
 * @since 1.8
 */
public class MPGenerator2 {


    @Test
    public void generateCode() {
        generateByTables();
    }

    /**
     * 包名
     */
    private static final String packageName = "com.rongzhe.house";
    /**
     * 模块名
     */
    private static final String moduleName = "order";

    /**
     * 作者
     */
    private static final String author = "ChengJIa";

    /**
     * 输出目录
     */
    private static  final File file=new File("");

    private static final String outPutDir = file.getAbsolutePath()+"/src/main/java";

    /**
     * 生成的表名 为空表示生产 所有
     */
    private static final String[] tableNames = {"order_bill_info"};


    /**
     * 数据库配置
     *
     * @return
     */
    private DataSourceConfig dataSource() {
        String dbUrl = "jdbc:mysql://rm-wz9rek243rz7c5k2ymo.mysql.rds.aliyuncs.com:3306/rongzhe?characterEncoding=utf8&useSSL=true";
        String username = "root";
        String driverName = "com.mysql.jdbc.Driver";
        String password = "BxMiUPLzha7QlYhjtO3!d4RVjH6oMvnw";
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername(username)
                .setPassword(password)
                .setDriverName(driverName);
        return dataSourceConfig;
    }


    /**
     * 设置包配置
     *
     * @return
     */
    private PackageConfig packageConfig() {
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(packageName)
                // controller包
//                .setController("controller")
                .setModuleName(moduleName)
                // 实体包
                .setEntity("entity")
                .setXml("mapper");

        return packageConfig;
    }

    /**
     * 全局配置
     *
     * @return
     */
    private GlobalConfig globalConfig() {
        GlobalConfig gc = new GlobalConfig();
        // 输出目录
        gc.setOutputDir(outPutDir);
        // 文件是否重写
        gc.setFileOverride(true);
        // 不需要ActiveRecord特性的请改为false
        gc.setActiveRecord(true);
        // XML 二级缓存
        gc.setEnableCache(false);
        // XML ResultMap
        gc.setBaseResultMap(true);
        // XML columList
        gc.setBaseColumnList(true);
        // .setKotlin(true) 是否生成 kotlin 代码
        gc.setAuthor(author);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        return gc;
    }

    /**
     * 策略
     *
     * @return
     */
    private StrategyConfig strategyConfig() {
        StrategyConfig strategy = new StrategyConfig();
        // 全局大写命名 ORACLE 注意
        // strategy.setCapitalMode(true);
        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 设置是否为lombak模式
        strategy.setEntityLombokModel(true);
        // 设置逻辑字段
//        strategy.setLogicDeleteFieldName("del_flag");
        strategy.setRestControllerStyle(true);
        // 此处可以修改为您的表前缀
        strategy.setTablePrefix(new String[]{""});
        //设置需要填充的字段
        List<TableFill> tableFillList = new ArrayList<>();
        tableFillList.add(new TableFill("create_time", FieldFill.INSERT));
        tableFillList.add(new TableFill("create_id", FieldFill.INSERT));
        tableFillList.add(new TableFill("update_time", FieldFill.INSERT_UPDATE));
        tableFillList.add(new TableFill("update_id", FieldFill.INSERT_UPDATE));
//        strategy.setTableFillList(tableFillList);
//         需要生成的表
        strategy.setInclude(tableNames);
        // 排除生成的表
        // strategy.setExclude(new String[]{"test"});
        // 自定义实体父类
//         strategy.setSuperEntityClass("com.ypfor.leaf.base.entity.AbstractDataEntity");
        // 自定义实体，公共字段
//         strategy.setSuperEntityColumns(new String[] { "id", "create_time","create_id","update_id","update_time","del_flag","remark" });
        // 自定义 mapper 父类
        // strategy.setSuperMapperClass("com.baomidou.demo.TestMapper");
        // 自定义 service 父类
//        strategy.setSuperServiceClass("com.ypfor.leaf.base.service.IBaseService");
        // 自定义 service 实现类父类
//        strategy.setSuperServiceImplClass("com.ypfor.leaf.base.service.impl.BaseService");
        // 自定义 controller 父类
        // strategy.setSuperControllerClass("com.baomidou.demo.TestController");

        // 【实体】是否生成字段常量（默认 false）
        // public static final String ID = "test_id";
        // strategy.setEntityColumnConstant(true);
        // 【实体】是否为构建者模型（默认 false）
        // public User setName(String name) {this.name = name; return this;}
        // strategy.setEntityBuilderModel(true);
        return strategy;
    }

    /**
     * 自定义模板
     *
     * @return
     */
    private TemplateConfig templateConfig() {
        TemplateConfig tc = new TemplateConfig();
        String TEMPLATE_ENTITY = "/templates/entity.java.vm";
        String TEMPLATE_MAPPER = "/templates/mapper.java.vm";
        String TEMPLATE_XML = "/templates/mapper.xml.vm";
        String TEMPLATE_SERVICE = "/templates/service.java.vm";
        String TEMPLATE_SERVICEIMPL = "/templates/serviceImpl.java.vm";
        String TEMPLATE_CONTROLLER = "/templates/controller.java.vm";
        tc.setController(TEMPLATE_CONTROLLER);
        tc.setEntity(TEMPLATE_ENTITY);
        tc.setMapper(TEMPLATE_MAPPER);
        tc.setXml(TEMPLATE_XML);
        tc.setService(TEMPLATE_SERVICE);
        tc.setServiceImpl(TEMPLATE_SERVICEIMPL);
//         如上任何一个模块如果设置 空 OR Null 将不生成该模块。
        return tc;
    }


    /**
     * 执行
     */
    private void generateByTables() {
        new AutoGenerator()
                //全局配置
                .setGlobalConfig(globalConfig())
                //数据库
                .setDataSource(dataSource())
                // 策略
                .setStrategy(strategyConfig())
                // 包信息
                .setPackageInfo(packageConfig())
                .setTemplate(templateConfig())
                // 执行
                .execute();
    }
}
