package com.rongzhe.house;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.baomidou.mybatisplus.mapper.Condition;
import com.rongzhe.house.banner.enums.BannerType;
import com.rongzhe.house.banner.service.BannerService;
import com.rongzhe.house.bill.entity.Bill;
import com.rongzhe.house.bill.mapper.BillMapper;
import com.rongzhe.house.common.exception.RongZheBusinessException;
import com.rongzhe.house.common.utils.DateUtil;
import com.rongzhe.house.common.utils.JPushUtil;
import com.rongzhe.house.contract.service.HouseReturnApplyService;
import com.rongzhe.house.dictionary.service.DictService;
import com.rongzhe.house.house.criteria.HouseCriteria;
import com.rongzhe.house.house.entity.HouseOverview;
import com.rongzhe.house.house.entity.HouseTraffic;
import com.rongzhe.house.house.es_repository.ESHouse;
import com.rongzhe.house.house.es_repository.ESHouseService;
import com.rongzhe.house.house.service.HouseService;
import com.rongzhe.house.message.criteria.HouseMsgCriteria;
import com.rongzhe.house.message.service.HouseMsgService;
import com.rongzhe.house.order.criteria.ItemCriteria;
import com.rongzhe.house.order.criteria.OrderCriteria;
import com.rongzhe.house.order.dao.OrderBillInfoMapper;
import com.rongzhe.house.order.entity.OrderBillInfo;
import com.rongzhe.house.order.enums.PayType;
import com.rongzhe.house.order.service.ItemService;
import com.rongzhe.house.order.service.OrderService;
import com.rongzhe.house.pay.AliPayConfig;
import com.rongzhe.house.pay.WxpayService;
import com.rongzhe.house.pay.entity.WeixinOrder;
import com.rongzhe.house.position.service.PositionService;
import com.rongzhe.house.repair.criteria.HouseRepairCriteria;
import com.rongzhe.house.repair.service.HouseRepairService;
import com.rongzhe.house.sms.AliyunSMSService;
import com.rongzhe.house.sys.dao.SysConfigMapper;
import com.rongzhe.house.tag.service.TagService;
import com.rongzhe.house.user.entity.UserHouseAppointment;
import com.rongzhe.house.user.service.UserHouseAppointmentService;
import com.rongzhe.house.user.service.UserService;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HouseApplicationTests {

    @Test
    public void contextLoads() {


    }

    @Autowired
    HouseService houseService;

    @Autowired
    HouseRepairService houseRepairService;

    @Autowired
    OrderBillInfoMapper orderBillInfoMapper;

    @Autowired
    BillMapper billMapper;

    @Test
    public void testOrder(){
        List<OrderBillInfo> list = orderBillInfoMapper.selectList(Condition.create().eq("order_code", "450611530170368000"));
        for(OrderBillInfo orderBillInfo:list){
            Bill bill = billMapper.selectById(orderBillInfo.getBillId());
            Date curr= new Date();
            bill.setUpdateTime(curr);
            bill.setReceived(bill.getPayable());
            bill.setReceivedDate(curr);
            bill.setStatus("2");//已收款

            //billMapper.updateById(bill);
        }
    }


    @Test
    public void queryHouseRepair() {
        HouseRepairCriteria houseRepairCriteria = new HouseRepairCriteria();
        System.out.println(JSON.toJSONString(houseRepairService.queryUserRepair(houseRepairCriteria, 40)));
    }

    @Test
    public void getCollectionHouse() {
        HouseCriteria houseCriteria = new HouseCriteria();
        houseCriteria.setCityId("5001");
        System.out.println(JSON.toJSONString(houseService.collectionGet(10, 10, 1)));
    }

    @Test
    public void getExcellentHouse() {
        HouseCriteria houseCriteria = new HouseCriteria();
        houseCriteria.setCityId("5001");
        System.out.println(JSON.toJSONString(houseService.getExcellentHouse(houseCriteria.getCityId())));
    }

    @Test
    public void queryHouseByCriteria() {
        HouseCriteria houseCriteria = new HouseCriteria();
        houseCriteria.setCityId("5001");
        houseCriteria.setEnterTime(new Date());
        System.out.println(JSON.toJSONString(houseService.queryHouseByCriteria(houseCriteria)));
    }

    @Test
    public void detail() {
        System.out.println(JSON.toJSONString(houseService.detail(82, 11)));
    }

    @Test
    public void getHouseRentPayConfigListByHouseId() {
        System.out.println(JSON.toJSONString(houseService.getHouseRentPayConfigListByHouseId(1)));
    }


    @Autowired
    private BannerService bannerService;

    @Test
    public void banner() {
        try {
            System.out.println(JSON.toJSONString(bannerService.getBannerListByType(BannerType.INDEX, "5001")));
        } catch (RongZheBusinessException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private PositionService positionService;

    @Test
    public void provinceList() {

        System.out.println(JSON.toJSONString(positionService.getProvinceList()));

    }

    @Test
    public void countyList() {

        System.out.println(JSON.toJSONString(positionService.getCountyListByProvinceId("5001")));

    }

    @Test
    public void routeList() {

        System.out.println(JSON.toJSONString(positionService.getHouseRouteListByProvinceId("50")));

    }


    @Autowired
    DictService dictService;

    @Test
    public void getDictByParentCode() {

        System.out.println(JSON.toJSONString(dictService.getDictByParentCode("House_Orientation")));

    }

    @Autowired
    TagService tagService;

    @Test
    public void getTagListByType() {

        System.out.println(JSON.toJSONString(tagService.getTagListByType(1)));

    }


    @Autowired
    UserHouseAppointmentService userHouseAppointmentService;

    @Test
    public void userHouseAppointmentService() {

        try {
            System.out.println(JSON.toJSONString(userHouseAppointmentService.getAppointmentByUid(8, 10, 1, 1)));
        } catch (RongZheBusinessException e) {
            e.printStackTrace();
        }


    }


    @Autowired
    private UserService userService;

    @Test
    public void getComplaintSuggest() {

        System.out.println(JSON.toJSONString(userService.getComplaintSuggest(1, 10, 1)));

    }


    @Test
    public void getIdentifyCardInfoByUid() {


        System.out.println(JSON.toJSONString(userService.getIdentifyCardInfoByUid(8)));


    }

    @Autowired
    private ESHouseService esHouseService;


    @Test
    public void ES_houseSave() throws IOException, ExecutionException, InterruptedException {


        HouseCriteria houseCriteria = new HouseCriteria();
        houseCriteria.setCityId("5001");
        houseCriteria.setEnterTime(new Date());

        List<HouseOverview> houseOverviews = houseService.queryHouseByCriteria(houseCriteria);

        List<ESHouse> esHouseList = new ArrayList<>();

        for (HouseOverview houseOverview : houseOverviews) {
            ESHouse esHouse = new ESHouse();

            esHouse.setId(houseOverview.getId());
            esHouse.setHouseName(houseOverview.getName());
            esHouse.setRegionName(houseOverview.getRegionName());
            esHouse.setCityId(houseOverview.getCityId());
            esHouse.setLat(houseOverview.getLatitude());
            esHouse.setLon(houseOverview.getLongitude());
            esHouse.setEntireRent(houseOverview.getEntireRent());
            esHouse.setOwnerRent(houseOverview.getOwnerRent());
            esHouse.setEnterTime(DateUtil.getFormatDateTime(houseOverview.getEnterTime(), "yyyy-MM-dd"));
            esHouse.setRoomNum(Integer.parseInt(houseOverview.getRoomNum()));
            esHouse.setRent(houseOverview.getRent());
            esHouse.setInsertTime(DateUtil.getFormatDateTime(houseOverview.getInsertTime(), "yyyy-MM-dd HH:mm:ss"));
            esHouse.setCountyId(houseOverview.getCountyId());
            esHouse.setLocation(new ArrayList<Double>() {
                {
                    add(Double.parseDouble(esHouse.getLat()));
                    add(Double.parseDouble(esHouse.getLon()));
                }
            });

            if (!houseOverview.getHouseTrafficList().isEmpty()) {
                List<String> routes = new ArrayList<>();
                Set<String> railList = new HashSet<>();
                Set<String> railStationList = new HashSet<>();
                for (HouseTraffic houseTraffic : houseOverview.getHouseTrafficList()) {
                    routes.add(houseTraffic.getTraffic());
                    railList.add(houseTraffic.getSubway());
                    railStationList.add(houseTraffic.getStation());
                }

                esHouse.setRouteList(routes);
                esHouse.setRailList(railList);
                esHouse.setRailStationList(railStationList);

                IndexRequest indexRequest = new IndexRequest("test_query", "condition_query", esHouse.getId().toString())
                        .source(jsonBuilder()
                                .startObject()
                                .endObject());
                UpdateRequest updateRequest = new UpdateRequest("test_query", "condition_query", esHouse.getId().toString())
                        .doc(jsonBuilder()
                                .startObject()


                                .field("name", esHouse.getHouseName())
                                .field("countyName", esHouse.getRegionName())
                                .field("cityId", esHouse.getCityId())
                                .field("countyId", esHouse.getCountyId())
                                .field("enterTime", esHouse.getEnterTime())
                                .field("isEntireRent", esHouse.isEntireRent())
                                .field("isOwnerRent", esHouse.isOwnerRent())
                                .field("roomNum", esHouse.getRoomNum())
                                .field("rent", esHouse.getRent())
                                .field("insertTime", esHouse.getInsertTime())

                                .latlon("location", esHouse.getLocation().get(0), esHouse.getLocation().get(1))

                                //.field("orientation")
                                .array("route", esHouse.getRouteList())
                                .array("railList", esHouse.getRailList())
                                .array("railStationList", esHouse.getRailStationList())


                                .endObject()); //如果不存在此文档 ，就增加 `indexRequest`

                System.out.println(client.update(updateRequest).get());
            }
        }
    }

    @Test
    public void getESHouse() {
        houseService.queryHouseByCriteria(new HouseCriteria());

    }


    @Autowired
    private Client client;

    @Test
    public void testEsClient() throws IOException, ExecutionException, InterruptedException {

        IndexRequest indexRequest = new IndexRequest("test_student", "student")
                .source(jsonBuilder()
                        .startObject()
                        .endObject());
        UpdateRequest updateRequest = new UpdateRequest("test_student", "student", "1")
                .doc(jsonBuilder()
                        .startObject()
                        .field("gender", "male")
                        .endObject())
                .upsert(indexRequest); //如果不存在此文档 ，就增加 `indexRequest`

        System.out.println(client.update(updateRequest).get());
    }

    @Test
    public void getEs() {


        SearchRequestBuilder srb1 = client
                .prepareSearch().setQuery(QueryBuilders.multiMatchQuery("Smith male", "name", "gender")).setFrom(0).setSize(1);

        MultiSearchResponse sr = client.prepareMultiSearch()
                .add(srb1)

                .get();

        // You will get all individual responses from MultiSearchResponse#getResponses()
        long nbHits = 0;
        for (MultiSearchResponse.Item item : sr.getResponses()) {
            SearchResponse response = item.getResponse();

            nbHits += response.getHits().getTotalHits();

            System.out.println(response);
        }

        System.out.println("nbHits:" + nbHits);

    }

    @Test
    public void dictGetAll() {
        System.out.println(JSON.toJSON(dictService.getAll()));
    }

    @Test
    public void houseAppointment() {


        UserHouseAppointment userHouseAppointment = new UserHouseAppointment();


        userHouseAppointment = JSON.parseObject("{\"contactTime\":\"2018-01-08\"}", UserHouseAppointment.class);
        System.out.println(userHouseAppointment);

    }


    @Autowired
    private AliPayConfig aliPayConfig;


    @Test
    public void createStr() throws AlipayApiException {
//实例化客户端
        AlipayClient alipayClient =
                new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
                        aliPayConfig.getAppId(),
                        aliPayConfig.getAppPrivateKey(),
                        "json", aliPayConfig.getCharset(),
                        aliPayConfig.getAlipayPublicKey(),
                        "RSA2");
//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
//SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody("我是测试数据");
        model.setSubject("App支付测试Java");
        model.setOutTradeNo("rz0000003");
        model.setTimeoutExpress("30m");
        model.setTotalAmount("0.01");
        model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        request.setNotifyUrl("商户外网可以访问的异步地址");
        try {
            //这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void alipayOrderQuery() throws AlipayApiException {

        AlipayClient alipayClient =
                new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
                        aliPayConfig.getAppId(),
                        aliPayConfig.getAppPrivateKey(),
                        "json", aliPayConfig.getCharset(),
                        aliPayConfig.getAlipayPublicKey(),
                        "RSA2");
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent("{" +
                "\"out_trade_no\":\"rz0000002\"" +
                "}");
        AlipayTradeQueryResponse response = alipayClient.execute(request);
        if (response.isSuccess()) {
            System.out.println(response.getBody());
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }


    @Autowired
    ItemService itemService;

    @Test
    public void itemList() {

        ItemCriteria itemCriteria = new ItemCriteria();
        itemCriteria.setBizId(38);
        //TODO 魔数
        itemCriteria.setBizType("1");

        System.out.println(JSON.toJSONString(itemService.list(itemCriteria)));
    }


    @Autowired
    private OrderService orderService;


    @Test
    public void createOrder() {
        try {
            orderService.createOrder(1, 1, 9);
        } catch (RongZheBusinessException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void getSian() {
        try {
            orderService.getSign("405132384854343680", PayType.ALI);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void paid() {
        orderService.paid("405165611715395584", "1", PayType.ALI.getType());
    }

    @Test
    public void orderList() {


        OrderCriteria orderCriteria = new OrderCriteria();
        orderCriteria.setUid(9);
        orderCriteria.setCurrentPage(1);
        orderCriteria.setPageSize(10);

        System.out.println(JSON.toJSONString(orderService.list(orderCriteria)));
    }


    @Test
    public void orderDetail() {


        OrderCriteria orderCriteria = new OrderCriteria();
        orderCriteria.setOrderCode("406473001832284160");

        System.out.println(JSON.toJSONString(orderService.detail(orderCriteria)));
    }


    @Autowired
    private WxpayService wxpayService;

    @Test
    public void preCreate() {
        WeixinOrder weixinOrder = new WeixinOrder();
        weixinOrder.setBody("荣者租房");
        weixinOrder.setOut_trade_no("rza001");
        weixinOrder.setTotal_fee(1);
        System.out.println(wxpayService.precreate(weixinOrder));

    }


    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Test
    public void sysConfig() {

        System.out.println(JSON.toJSONString(sysConfigMapper.selectConfig()));
    }

    @Autowired
    AliyunSMSService aliyunSMSService;

    @Test
    public void sms() {
        aliyunSMSService.sendSms("18502861390", "SMS_123535027", "1234");
    }


    @Autowired
    private HouseReturnApplyService houseReturnApplyService;

    @Test
    public void returnDetail() {
        System.out.println(JSON.toJSONString(houseReturnApplyService.selectByContractId(1)));
    }

    @Test
    public void contractList() {
        System.out.println(JSON.toJSONString(userService.contractList(1, 10, "49")));
    }


    @Autowired
    private HouseMsgService houseMsgService;

    @Test
    public void msgList() {

        HouseMsgCriteria houseMsgCriteria = new HouseMsgCriteria();
        houseMsgCriteria.setResponseId(1);
        System.out.println(JSON.toJSONString(houseMsgService.list(houseMsgCriteria)));


    }


    @Autowired
    private JPushUtil jPushUtil;




    @Test
    public void jpushTest(){
        try {
            jPushUtil.sendWithDevice("860482033184119",null,"哟哟哟");
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
    }
}
